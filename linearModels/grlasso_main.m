function grlasso_main(expName,kFolds)
% GRLASSO_MAIN - group lasso regression results using PASPAL
%
% INPUTS
%   expName - name of experiment
%   kfolds - integer number of cross validation samples (default=5)
%
% OUTPUTS
%   saves group lasso results to Experiments/Results/expName
%
% EXAMPLE: grlasso_main('BH_001',5)
%
% CREATED: MG 25/12/2017

tic
fprintf('Running group lasso regression for %s',expName);

%% fill in optional parameters
if ~exist('kfolds','var') || isempty(kfolds),
  kfolds = 5;
end

load(fullfile('Experiments/Data/',expName));
if ~exist('blocks','var')
  error('Missing block specification for creating groups.')
end

% reproducible random splits
rng(2017)
% cross validation
protocol = 'both'; % sparsity learning followed by ridge debiasing
[output,model] =grl_kcv(train.X,train.y,'blocks',blocks,'plot',false,'K',kFolds,'smooth_par',0,'protocol',protocol,'expName',expName);
% testing
grlss.preds.train = l1l2_pred(model,train.X,train.y,'regr');
grlss.preds.test = l1l2_pred(model,test.X,test.y,'regr');

grlss.output = output;
grlss.model = model;

grlss.elapsedTime=toc;

% save results
resFile = fullfile('Experiments/Results',[expName,'.mat']);
if exist(resFile, 'file') == 2
  save(resFile,'grlss','-append');
else
  save(resFile,'grlss');
end

fprintf(', elapsed time = %f\n',toc);

end