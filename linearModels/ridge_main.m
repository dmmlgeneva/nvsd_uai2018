function ridge_main(expName,kFolds)
% RIDGE_MAIN - ridge regression results
%
% INPUTS
%   expName - name of experiment
%   kfolds - integer number of cross validation samples (default=5)
%
% OUTPUTS
%   saves ridge results to Experiments/Results/expName
%
% EXAMPLE: ridge_main('BH_001',5)
%
% CREATED: MG 25/12/2017

tic
fprintf('Running ridge regression for %s',expName);

%% fill in optional parameters
if ~exist('kfolds','var') || isempty(kfolds),
  kfolds = 5;
end

load(fullfile('Experiments/Data/',expName));

% 50-long lambda grid
sigma = normest(train.X'*train.X);
lambdas = sigma*(10.^(-9.8:.2:0));

% cross validation
rdg = ridge_crossVal(train.X,train.y,lambdas,kFolds,expName);
rdg.lambdas = lambdas;

% test results
[rdg.preds.train,rdg.errs.train] = predict_Linear(train.X,train.y,rdg.params);
[rdg.preds.test,rdg.errs.test] = predict_Linear(test.X,test.y,rdg.params);

rdg.elapsedTime=toc;

% save results
resFile = fullfile('Experiments/Results',[expName,'.mat']);
if exist(resFile, 'file') == 2
  save(resFile,'rdg','-append');
else
  save(resFile,'rdg');
end

fprintf(', elapsed time = %f\n',toc);

end