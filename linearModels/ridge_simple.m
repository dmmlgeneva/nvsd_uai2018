function [w] = ridge_simple(X,y,lambda)
% RIDGE_SIMPLE - ridge regression solution for single hyper-parameter value
%
% INPUTS
%   X - input data matrix
%   y - output vector
%   lambda - hyper-parameter value
%
% OUTPUTS
%   w - ridge parameters
%
% EXAMPLE: [w] = ridge_simple(X,y,lambda)
%
% CREATED: MG 25/12/2017

[n,d] = size(X);
w = (X'*X + n*lambda*eye(d))\(X'*y);

end
  
