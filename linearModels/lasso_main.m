function lasso_main(expName,kFolds)
% LASSO_MAIN - lasso regression results using PASPAL
%
% INPUTS
%   expName - name of experiment
%   kfolds - integer number of cross validation samples (default=5)
%
% OUTPUTS
%   saves lasso results to Experiments/Results/expName
%
% EXAMPLE: lasso_main('BH_001',5)
%
% CREATED: MG 25/12/2017

tic
fprintf('Running lasso regression for %s',expName);

%% fill in optional parameters
if ~exist('kfolds','var') || isempty(kfolds),
  kfolds = 5;
end

load(fullfile('Experiments/Data/',expName));

% reproducible random splits
rng(2017)
% cross validation
protocol = 'both'; % sparsity learning followed by ridge debiasing
[output,model] = l1l2_kcv(train.X,train.y,'plot',false,'K',kFolds,'smooth_par',0,'protocol',protocol,'expName',expName);

% testing
lss.preds.train = l1l2_pred(model,train.X,train.y,'regr');
lss.preds.test = l1l2_pred(model,test.X,test.y,'regr');

lss.output = output;
lss.model = model;

lss.elapsedTime=toc;

% save results
resFile = fullfile('Experiments/Results',[expName,'.mat']);
if exist(resFile, 'file') == 2
  save(resFile,'lss','-append');
else
  save(resFile,'lss');
end

fprintf(', elapsed time = %f\n',toc);

end