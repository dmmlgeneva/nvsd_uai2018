function mean_main(expName)
% MEAN_MAIN - simple mean prediction
%
% INPUTS
%   expName - name of experiment
%
% OUTPUTS
%   saves simple mean prediction results to Experiments/Results/expName
%
% EXAMPLE: mean_main('BH_001',5)
%
% CREATED: MG 25/12/2017

tic
fprintf('Getting mean predictions for %s',expName);

load(fullfile('Experiments/Data/',expName));

% test results
[mn.preds.train,mn.errs.train] = predict_Linear(train.X,train.y,zeros(size(train.X,2),1));
[mn.preds.test,mn.errs.test] = predict_Linear(test.X,test.y,zeros(size(train.X,2),1));

mn.elapsedTime=toc;

% save results
resFile = fullfile('Experiments/Results',[expName,'.mat']);
if exist(resFile, 'file') == 2
  save(resFile,'mn','-append');
else
  save(resFile,'mn');
end

fprintf(', elapsed time = %f\n',toc);

end