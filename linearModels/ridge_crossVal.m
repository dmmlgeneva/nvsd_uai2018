function [cvRes] = ridge_crossVal(X,y,lambdas,kfolds,expName)
% RIDGE_REGPATH - cross validate ridge regression solution
%
% INPUTS
%   X - train input matrix
%   y - train output vector
%   lambdas - vector of hyper-parameter values
%   kfolds - integer number of cross validation samples (default=5)
%   doPlots - if 1, do plots of regularization path (default=0)
% INPUTS OPTIONAL
%   expName - experiment name to load validation data from
%
% OUTPUTS
%   cvResults - struct with fields
%     avgErrs.train - avg cv MSE for train data for all lambdas
%     avgErrs.test - avg cv MSE for test data for all lambdas
%     lbdOptIdx - index of lambda optimal in lambdas vector (minimising MSE over test)
%
% EXAMPLE: [cvRes,finalRes] = ridge_corssVal(X,y,lambdas,5,1)
%
% CREATED: MG 25/12/2017

%% fill in optional parameters
if ~exist('kfolds','var') || isempty(kfolds),
  kfolds = 5;
end

[n,d] = size(X);
ll = length(lambdas);

%% Cross validation
% create the cv splits
cvFolds = splitCV(X,y,kfolds,expName);
% get errors for the regularization path
cvErrs.train = zeros(kfolds,ll);
cvErrs.test = zeros(kfolds,ll);
for i=1:kfolds
  w = ridge_regPath(cvFolds(i).train.X,cvFolds(i).train.y,lambdas);
  [~,cvErrs.train(i,:)] = predict_Linear(cvFolds(i).train.X,cvFolds(i).train.y,w);
  [~,cvErrs.test(i,:)] = predict_Linear(cvFolds(i).test.X,cvFolds(i).test.y,w);
end
% get the avg errors and optimal lambda from avg cv test errors
cvRes.avgErrs.train = mean(cvErrs.train);
cvRes.avgErrs.test = mean(cvErrs.test);
cvRes.lbdOptIdx = find(cvRes.avgErrs.test==min(cvRes.avgErrs.test),1,'last');
cvRes.lbdOpt = lambdas(cvRes.lbdOptIdx);

%% final model
cvRes.params = ridge_simple(X,y,cvRes.lbdOpt);


end


