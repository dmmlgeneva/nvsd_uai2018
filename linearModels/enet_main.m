function enet_main(expName,kFolds)
% ENET_MAIN - elastic net regression results using PASPAL
%
% INPUTS
%   expName - name of experiment
%   kfolds - integer number of cross validation samples (default=5)
%
% OUTPUTS
%   saves elastic net results to Experiments/Results/expName
%
% EXAMPLE: enet_main('BH_001',5)
%
% CREATED: MG 25/12/2017

tic
fprintf('Running enet regression for %s',expName);

%% fill in optional parameters
if ~exist('kfolds','var') || isempty(kfolds),
  kfolds = 5;
end

load(fullfile('Experiments/Data/',expName));

% reproducible random splits
rng(2017)
% cross validation
protocol = 'both'; % sparsity learning followed by ridge debiasing

% add an extra cv step to select the best smooth_par
smooth_pars = [0.1;0.3;0.5;0.7;0.9]; % hyper-parameter for l2 part in l1l2 combination
for i = 1:length(smooth_pars)
  [outTemp(i),modelTemp(i)] =l1l2_kcv(train.X,train.y,'plot',false,'K',kFolds,'smooth_par',smooth_pars(i),'protocol',protocol,'expName',expName);
  kcv_1step(i) = outTemp(i).err_KCV_1step(outTemp(i).t_opt);
  kcv_2steps(i) = outTemp(i).err_KCV_2steps(outTemp(i).t_opt2,outTemp(i).l_opt2);
end
% select best smooth_par
smpar_opt = find(kcv_1step==min(kcv_1step),1);
smpar_opt2 = find(kcv_2steps==min(kcv_2steps),1);
output = outTemp(smpar_opt2);
model = modelTemp(smpar_opt2);
output.selected_all = outTemp(smpar_opt).selected_all;
output.sparsity = outTemp(smpar_opt).sparsity;
output.t_opt = outTemp(smpar_opt).t_opt;
output.tau_top_1step = outTemp(smpar_opt).tau_opt_1step;
output.err_KCV_1step = outTemp(smpar_opt).err_KCV_1step;
output.smpar_opt = smpar_opt;
output.smpar_opt2 = smpar_opt2;
model.beta_1step = modelTemp(smpar_opt).beta_1step;
model.offset_1step = modelTemp(smpar_opt).offset_1step;
model.selected_1step = modelTemp(smpar_opt).selected_1step;

% testing
enet.preds.train = l1l2_pred(model,train.X,train.y,'regr');
enet.preds.test = l1l2_pred(model,test.X,test.y,'regr');

enet.output = output;
enet.model = model;

enet.elapsedTime=toc;

% save results
resFile = fullfile('Experiments/Results',[expName,'.mat']);
if exist(resFile, 'file') == 2
  save(resFile,'enet','-append');
else
  save(resFile,'enet');
end

fprintf(', elapsed time = %f\n',toc);

end