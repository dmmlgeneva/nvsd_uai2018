function [w] = ridge_regPath(X,y,lambdas)
% RIDGE_REGPATH - ridge regression solutions for multiple hyper-parameter values
%
% INPUTS
%   X - input data matrix
%   y - output vector
%   lambdas - vector of hyper-parameter values
%
% OUTPUTS
%   w - ridge parameters, each column correspods to 1 lambda
%
% EXAMPLE: [w] = ridge_regPath(X,y,lambdas)
%
% CREATED: MG 25/12/2017

[n,d] = size(X);
ll = length(lambdas);
w = zeros(d,ll);

if n>d
  [V,D] = eig(X'*X);
  VXy = V'*X'*y;
  for i=1:ll
    w(:,i) = V*diag((diag(D)+n*lambdas(i)).^(-1))*VXy;
  end
else
  [V,D] = eig(X*X');
  XV = X'*V;
  Vy = V'*y;
  for i=1:ll
    w(:,i) = XV*diag((diag(D)+n*lambdas(i)).^(-1))*Vy;
  end
end

end
  
