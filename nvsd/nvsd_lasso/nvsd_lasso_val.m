function cvRes = nvsd_lasso_val(train,valid,taus,kernelType,kernelParam,nu,step1)
% NVSD_LASSO_VAL - train nvsd lasso model and use validation sample to pick hyper-params
%
% INPUTS
%   train - struct with X and y fields
%   valid - struct with X and y fields
%   taus - vector of hyper-parameter values
%   kernelType - type kernel (pol,polHom,gauss,lin)
%   kernelParam - kernel parameter (specific for kernel type)
%   nu - hilbert norm hyper-param
%   step1 - step1 learning (always finish full tau grid, otherwise stop when learning full models)
%
% OUTPUTS
%   cvRes - cross-validation results struct with fields
%     avgErrs.train - avg cv MSE for train data for all lambdas
%     avgErrs.test - avg cv MSE for test data for all lambdas
%     tauOptIdx - index of tau optimal in lambdas vector (minimising MSE over test)
%     tauOpt - tau optimal value
%     omg - final model concatenated parameters
%     pdNorm - train sample partial derivative norms
%     params.a - final model parameters
%     params.b - final model parameters
%     avgErrs...pdNorm exist also for _step2 version
%     params.a_lss_step2, params.b_lss_step2 - final lss model parameters from step2 (corresponds to omg_step2)
%     params.a_step2 - step2 results for kr_ridge
%
% EXAMPLE: cvRes = nvsd_lasso_corssVal(X,y,taus,5,'gauss',0.5)
%
% CREATED: MG 6/1/2018

% hyper-parameters
tt = length(taus);
n = size(train.X,1);

% 2nd step 100-long lambda grid
sigma = normest(train.X'*train.X);
lambdas = sigma*(10.^(-9.8:.2:0));

%% Train models
% initiate matrices
cvErrs.train_step2 = zeros(tt,length(lambdas));
cvErrs.valid_step2 = zeros(tt,length(lambdas));
% 1st step fitting by nvsd lasso
fprintf('nvsd lasso: start training ... \n');
trainTic = tic;
[K,~,~,~,D,~,F,Z,~,Q] = nvsd_kernelMatrices(train.X,train.X,kernelType,kernelParam);
if nu==0 % save memory
  Q=0;
end
[omg,pdNorm,iter,admmTime] = nvsd_lasso_regPath(F,Z,Q,train.y,taus,nu,step1);
a = omg(1:size(K,1),:); b=omg(size(K,1)+1:end,:);
fprintf('nvsd lasso: finished training, elapsed Time %f \n', toc(trainTic));
[~,cvErrs.train] = predict_Kr(K,train.y,a,D,b);
[K,~,~,~,D] = nvsd_kernelMatrices(train.X,valid.X,kernelType,kernelParam);
[~,cvErrs.valid] = predict_Kr(K,valid.y,a,D,b);

% 2nd step fitting by kernel ridge
% train ridge models
sparsePattern = logical(zeros(size(pdNorm)));
for tIdx = 1:tt;
  sparsePattern(:,tIdx) = pdNorm(:,tIdx)>0;
  [~,interIdx] = intersect(sparsePattern',sparsePattern(:,tIdx)','rows');
  if interIdx>0 && interIdx~=tIdx
    % copy results if same sparse pattern
    ar{tIdx} = ar{interIdx};
    cvErrs.train_step2(tIdx,:) = cvErrs.train_step2(interIdx,:);
    cvErrs.valid_step2(tIdx,:) = cvErrs.valid_step2(interIdx,:);
  elseif sum(sparsePattern(:,tIdx))==0
    % mean prediction for completely sparse model
    ar{tIdx} = zeros(n,length(lambdas));
    [~,cvErrs.train_step2(tIdx,:)] = predict_Linear(train.X,train.y,zeros(size(train.X,2),1));
    [~,cvErrs.valid_step2(tIdx,:)] = predict_Linear(valid.X,valid.y,zeros(size(valid.X,2),1));
  else
    % kr_ridge model
    K = kernelMatrices(train.X(:,sparsePattern(:,tIdx)),train.X(:,sparsePattern(:,tIdx)),kernelType,kernelParam);
    ar{tIdx} = kr_ridge_regPath(K,train.y,lambdas);
    [~,cvErrs.train_step2(tIdx,:)] = predict_Kr(K,train.y,ar{tIdx});
    K = kernelMatrices(train.X(:,sparsePattern(:,tIdx)),valid.X(:,sparsePattern(:,tIdx)),kernelType,kernelParam);
    [~,cvErrs.valid_step2(tIdx,:)] = predict_Kr(K,valid.y,ar{tIdx});
  end
end

cvRes.cvFolds.pdNorm = pdNorm;
cvRes.cvFolds.iter = iter;
cvRes.cvFolds.admmTime = admmTime;
cvRes.cvFolds.cvErrs = cvErrs;
%% 1st step avg errors and optimal tau from avg validation errors
cvRes.tauOptIdx = find(cvErrs.valid==min(cvErrs.valid),1,'last');
cvRes.tauOpt = taus(cvRes.tauOptIdx);

%% 2nd step avg errors and optimal tau from avg validation errors
% min valid errror across tau lambda
minValidError_step2 = min(min(cvErrs.valid_step2));
% optimal tau lbda indexes (work over transpose to give preference to higher tau)
minIdxT = find(cvErrs.valid_step2'==minValidError_step2,1,'last');
[cvRes.lbdOptIdx_step2,cvRes.tauOptIdx_step2] = ind2sub(size(cvErrs.valid_step2'),minIdxT);
cvRes.tauOpt_step2 = taus(cvRes.tauOptIdx_step2);
cvRes.lbdOpt_step2 = lambdas(cvRes.lbdOptIdx_step2);
fprintf('minIdxT=%f lbd=%d tau=%d \n', minIdxT, cvRes.lbdOptIdx_step2,cvRes.tauOptIdx_step2);

%% final models
% 1st step 
cvRes.omg = omg(:,cvRes.tauOptIdx);
cvRes.pdNorm = pdNorm(:,cvRes.tauOptIdx);
cvRes.params.a = cvRes.omg(1:n);
cvRes.params.b = cvRes.omg(n+1:end);

% 2nd step 
cvRes.omg_step2 = omg(:,cvRes.tauOptIdx_step2);
fprintf('omg=%d %d omg_step2=%d \n', size(omg,1), size(omg,2), length(cvRes.omg_step2));
cvRes.pdNorm_step2 = pdNorm(:,cvRes.tauOptIdx_step2);
cvRes.params.a_lss_step2 = cvRes.omg_step2(1:n);
cvRes.params.b_lss_step2 = cvRes.omg_step2(n+1:end);
cvRes.params.a_step2 = ar{cvRes.tauOptIdx_step2}(:,cvRes.lbdOptIdx_step2);

end


