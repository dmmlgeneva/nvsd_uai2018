function nvsd_lasso_main(expName,kFolds,kernelType,kernelParam,nu,tau_max,step1)
% NVSD_LASSO_MAIN - nvsd lasso results
%
% INPUTS
%   expName - name of experiment
%   kfolds - integer number of cross validation samples (default=5)
%   kernelType - type kernel (pol,polHom,gauss,lin)
%   kernelParam - kernel parameter (specific for kernel type)
%   nu - hilbert norm hyper-param
%   step1 - step1 learning (always finish full tau grid, otherwise stop when learning full models)
%
% OUTPUTS
%   saves nvsd lasso results to Experiments/Results/expName
%
% EXAMPLE: nvsd_lasso_main('BH_001',5,'gauss')
%
% CREATED: MG 26/12/2017

mainTic = tic;
fprintf('Running nvsd lasso for %s\n',expName);

%% fill in optional parameters
if ~exist('nu','var') || isempty(nu),
  nu = 0;
end
if ~exist('step1','var') || isempty(step1),
  step1 = 0;
end


load(fullfile('Experiments/Data/',expName));
[n,d] = size(train.X);

%% fill in kernel Param
% guaussian width
if (strcmp(kernelType,'gauss') || strcmp(kernelType,'gausn')) && (~exist('kernelParam','var') || isempty(kernelParam)),
  kernelParam = gaussWidth(train.X,train.y);
% empty for linear kernel
elseif strcmp(kernelType,'lin')
  kernelParam = [];
end
kPOrig = kernelParam;
if strcmp(kernelType,'gausn')
  kernelParam = kernelParam/d;
end
%% tau_max
if ~exist('tau_max','var') || isempty(tau_max),
  % train sample kernel matrices
  [~,~,~,~,~,~,F,Z,zaNorm] = nvsd_kernelMatrices(train.X,train.X,kernelType,kernelParam);
  tau_max = zaNorm*norm(F\train.y)/sqrt(n);
end
% tau grid
taus = tauGrid(tau_max);
%taus = tauGrid(tau_max,[],5);

% cross validation
%nvsd_lss = nvsd_lasso_crossVal(train.X,train.y,taus,kFolds,kernelType,kernelParam,expName);

% hold-out validation
nvsd_lss = nvsd_lasso_val(train,valid,taus,kernelType,kernelParam,nu,step1);
nvsd_lss.taus = taus;

%% test results
% 1st step
[K,~,~,~,D] = nvsd_kernelMatrices(train.X,train.X,kernelType,kernelParam);
[nvsd_lss.preds.train,nvsd_lss.errs.train] = predict_Kr(K,train.y,nvsd_lss.params.a,D,nvsd_lss.params.b);
[K,~,~,~,D] = nvsd_kernelMatrices(train.X,test.X,kernelType,kernelParam);
[nvsd_lss.preds.test,nvsd_lss.errs.test] = predict_Kr(K,test.y,nvsd_lss.params.a,D,nvsd_lss.params.b);
% 2nd step
if sum(nvsd_lss.params.a_step2)==0
  % mean prediction for completely sparse model
  [nvsd_lss.preds.train_step2,nvsd_lss.errs.train_step2] = predict_Linear(train.X,train.y,zeros(size(train.X,2),1));
  [nvsd_lss.preds.test_step2,nvsd_lss.errs.test_step2] = predict_Linear(test.X,test.y,zeros(size(test.X,2),1));
else
  % kr_ridge model
  K = kernelMatrices(train.X(:,nvsd_lss.pdNorm_step2>0),train.X(:,nvsd_lss.pdNorm_step2>0),kernelType,kernelParam);
  [nvsd_lss.preds.train_step2,nvsd_lss.errs.train_step2] = predict_Kr(K,train.y,nvsd_lss.params.a_step2);
  K = kernelMatrices(train.X(:,nvsd_lss.pdNorm_step2>0),test.X(:,nvsd_lss.pdNorm_step2>0),kernelType,kernelParam);
  [nvsd_lss.preds.test_step2,nvsd_lss.errs.test_step2] = predict_Kr(K,test.y,nvsd_lss.params.a_step2);
end

nvsd_lss.kernelParam=kPOrig;
nvsd_lss.nu=nu;
nvsd_lss.elapsedTime_cv=toc(mainTic);

%% choose the kernel function and save results
switch kernelType
  case 'pol'
    nvsd_lss_pol = nvsd_lss;
    % save results
    resFile = fullfile('Experiments/Results',[expName,'.mat']);
    if exist(resFile, 'file') == 2
      save(resFile,'nvsd_lss_pol','-append');
    else
      save(resFile,'nvsd_lss_pol');
    end
  case 'polHom'
    nvsd_lss_polHom = nvsd_lss;
    % save results
    resFile = fullfile('Experiments/Results',[expName,'.mat']);
    if exist(resFile, 'file') == 2
      save(resFile,'nvsd_lss_polHom','-append');
    else
      save(resFile,'nvsd_lss_polHom');
    end
  case 'gauss'
    nvsd_lss_gauss = nvsd_lss;
    % save results
    resFile = fullfile('Experiments/Results',[expName,'.mat']);
    if exist(resFile, 'file') == 2
      save(resFile,'nvsd_lss_gauss','-append');
    else
      save(resFile,'nvsd_lss_gauss');
    end
  case 'gausn'
    nvsd_lss_gausn = nvsd_lss;
    % save results
    resFile = fullfile('Experiments/Results',[expName,'.mat']);
    if exist(resFile, 'file') == 2
      save(resFile,'nvsd_lss_gausn','-append');
    else
      save(resFile,'nvsd_lss_gausn');
    end
  case 'lin'
    nvsd_lss_lin = nvsd_lss;
    % save results
    resFile = fullfile('Experiments/Results',[expName,'.mat']);
    if exist(resFile, 'file') == 2
      save(resFile,'nvsd_lss_lin','-append');
    else
      save(resFile,'nvsd_lss_lin');
    end
  otherwise
    error('unknown kernel')
end


fprintf('Total time = %f\n',toc(mainTic));

end