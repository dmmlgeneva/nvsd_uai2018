function [omg,pdNorm,iter,admmTime] = nvsd_grlasso_regPath(F,Z,Q,y,blocks,taus,nu,step1)
% NVSD_GRLASSO_REGPATH - nvsd group lasso regression solutions for multiple hyper-parameter values
%
% INPUTS
%   F,Z,Q - kernel matrices
%   y - output vector
%   blocks - blocks for grouping variables
%   taus - vector of tau hyper-parameter values
%   nu - hilbert norm hyper-param
%   step1 - step1 learning (always finish full tau grid, otherwise stop when learning full models)
%
% OUTPUTS
%   omg - nvsd lasso parameters, each column correspods to 1 tau
%   pdNorms - traning sample estimates of the partial derivative norms, each column correspods to 1 tau
%   iter - number of admm iterations
%   admmTime - admm elapsed time
%
% EXAMPLE: [omg,pdNorm,iter,admmTime] = nvsd_lasso_regPath(F,Z,y,taus,'gauss',0.5)
%
% CREATED: MG 30/12/2017

% stoping criteria
stopCrit.maxIter = 1000;
% abs and real factors for primal and dual ptimality
stopCrit.eabs = 1e-4;
stopCrit.erel=1e-4;
%stopCrit.eabs = 1e-6; % lower for exp6
%stopCrit.erel=1e-6; % lower for exp6
% initial parameter values
initVal.omg = pinv(F)*y;
initVal.phi = Z*initVal.omg;
initVal.rho = 0.1;

% comple regularization path
tt = length(taus);
n = size(F,1); d=(size(F,2)-n)/n;
omg = ones(size(F,2),tt);
pdNorm = ones(d,tt);
countFullpdNorms = 0;
for tIdx = 0:tt-1;
  tau = taus(length(taus)-tIdx);
  fprintf('nvsd grlasso: training tauIdx %d ... ',tIdx);
  [omg(:,tt-tIdx),pdNorm(:,tt-tIdx),iter(:,tt-tIdx),admmTime(:,tt-tIdx)] = nvsd_grlasso_algo(F,Z,Q,y,blocks,tau,nu,initVal,stopCrit);
  initVal.omg=omg(:,tt-tIdx);
  initVal.phi = Z*initVal.omg;
  % how many times have all dimensions been selected
  if sum(logical(pdNorm(:,tt-tIdx)))==d;
    countFullpdNorms = countFullpdNorms+1;
  else 
    countFullpdNorms = 0;
  end
  fprintf('admm iterations %d, time %f \n',iter(:,tt-tIdx),admmTime(:,tt-tIdx));
  if ~step1 && countFullpdNorms==5
    fprintf('nvsd grlasso: full models so stop at iteration %d \n',tIdx);
    break
  end
end


end
  
