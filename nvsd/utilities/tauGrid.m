function taus = tauGrid(tau_max,tau_min,tau_length)
% TAUGRID - get tau grid as a geometric series
%
% CREATED: MG 26/12/2017

%% fill in optional parameters
if ~exist('tau_min','var') || isempty(tau_min),
  tau_min = tau_max/1000;
end
if ~exist('tau_length','var') || isempty(tau_length),
  tau_length = 50;
end

taus = [tau_min tau_min*((tau_max/tau_min)^(1/(tau_length-1))).^(1:(tau_length-1))]; %geometric series.

end