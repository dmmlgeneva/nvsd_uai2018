function [preds,errs] = predict_Linear(X,y,w)
% PREDICT_LINEAR - predict linear model y=XW
%
% INPUTS
%   X - input data matrix
%   y - output vector
%   w - parameter values (each column can have different param estimate)
%
% OUTPUTS
%   preds - prediction values
%   errs - prediction errors
%
% EXAMPLE: [preds,errs] = predict_Linear(X,y,w)
%
% CREATED: MG 25/12/2017

% predictions
preds = X*w;
% prediction errors (MSE)
errs = sum((bsxfun(@minus,preds,y)).^2)/size(X,1);

end