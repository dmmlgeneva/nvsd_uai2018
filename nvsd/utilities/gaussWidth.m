function width = gaussWidth(X,y,k)
% GAUSSWIDTH - estimate gauss width
%   as median from the distance between half nearest neihbours
%   (similar to Rosasco2013 real experiments)
%
% INPUTS
%   X - input data matrix
%   y - outputs
% INPUTS OPTIONAL
%   k - # of nearest neihbours
%
% OUTPUTS
%   width - estimated gauss kernel width
%
% EXAMPLE: width = gaussWidth(X)
%
% CREATED: MG 26/12/2017

%% fill in optional parameters
if ~exist('k','var') || isempty(k),
  k = min(20,length(y));
end

% get nearest neighbours
idy = knnsearch(y,y,'K',k,'NSMethod','exhaustive');
% linear indexes
n = size(idy,1);
idx = repmat([1:n]',1,size(idy,2));
idxLin = sub2ind([n,n],idx(:),idy(:));

% distance between instances
rowsq = sum(X.^2,2);
prodX = 2*X*X';
distMat = bsxfun(@minus,rowsq,prodX);
distMat = bsxfun(@plus,distMat,rowsq');
distMat = sqrt(distMat);

% nearest distances
nnDistMat = reshape(distMat(idxLin),n,[]);
width = round(mean(median(nnDistMat(:,2:end),2)),1);

% distMat = sort(distMat,2);
% distMat = reshape(distMat(2:floor(size(X,1)/2)+1,:),[],1);
% median from half of nearest points
% width = median(distMat);
% widthVec = quantile(distMat,[0.25 0.5 0.75]);
% width = quantile(distMat,[0.25]);

end


