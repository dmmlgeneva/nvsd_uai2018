function [derNorms,derVals] = pders_Kr(Da,a,La,b)
% PDERS_KR - function partial derivative norms (eq. 7 of the paper)
%
% INPUTS
%   Da, Lab - matrices of kernel partial derivatives
%   a,b - kernel parameter values (each column can have different param estimate)
%
% OUTPUTS
%   derNorms - partial derivative norms
%   derVals - partial derivative values evaluated at sample points
%
% EXAMPLE: [derNorms,derVals] = pders_Kr(Da,a,Lab,b)
%
% CREATED: MG 2/1/2018

%% fill in optional parameters
m = size(a,2);
d = size(Da,3);
n = size(Da,1);
if ~exist('La','var') || isempty(La),
  La = zeros(1,1,d);
end
if ~exist('b','var') || isempty(b),
  b = zeros(1,m);
end

% get sparsity
for mIdx = 1:m
  for i = 1:d
    derVals = Da(:,:,i)*a(:,mIdx) + [La(:,:,i)]'*b(:,mIdx); % derivative values for each dimension
    derNorms(i,mIdx) = sqrt(sum(derVals.^2))/sqrt(n); % average norm of the derivatives across instances for each dimension
  end
end

end


