function [preds,errs] = predict_Kr(K,y,a,D,b)
% PREDICT_KR - predict kernel model y=K'*a+D'*b
%
% INPUTS
%   K - n1 x n2 kernel matrix
%   y - output vector
%   a - kernel parameter values (each column can have different param estimate)
%   D - n1 x n2 kernel matrix
%   y - output vector
%   a - kernel parameter values (each column can have different param estimate)
%
% OUTPUTS
%   preds - prediction values
%   errs - prediction errors
%
% EXAMPLE: [preds,errs] = predict_Kr(K,y,a,D,b)
%
% CREATED: MG 25/12/2017

%% fill in optional parameters
if ~exist('D','var') || isempty(D),
  D = 0;
end
if ~exist('b','var') || isempty(b),
  b = 0;
end

% predictions
preds = K'*a + D'*b; % for each model in column
% prediction errors (MSE)
errs = sum((bsxfun(@minus,preds,y)).^2)/length(y);

end