function [K,Da,Da1,La,D,L,F,Z,zaNorm,Q] = nvsd_kernelMatrices(xTrain,xTest,kernelType,kernelParam)
% NVSD_KERNELMATRICES - get nvsd kernal matrices
%
% INPUTS
%   xTrain - train input maatrix n1 x d
%   xTest - test input matrix n2 x d (or train again)
%   kernelType - type kernel (pol,polHom,gauss,lin)
%   kernelParam - kernel parameter (specific for kernel type)
%
% OUTPUTS
%   K - n1 x n2 gram matrix with elements K(i,j) = kFun(xi,xj)
%   Da - n1 x n2 x d matrix of kernel derivatives with elements Da(i,j,a) = partial(kFun(xi,xj))/partial(xi_a)
%   Da1 - n2 x n1 x d matrix same as Da but xi is xTest and xj is xTrain
%   La - n1 x n2d matrix of kernel 2nd derivatives 
%   D,L,F,Z,Q - concatenations as per the paper
%   zaNorm - used to get tauMax
%     
% EXAMPLE: [K,Da,Da1,La,D,L,F,Z] = nvsd_kernelMatrices(xTrain,xTest,'gauss',0.5)
%
% CREATED: MG - 26/12/2017

%% fill in optional parameters
if ~exist('blocks','var') || isempty(blocks),
  blocks{1} = [1:size(xTrain,2)]; % treat all data as a single block
end

%% train kernel matrices
[n,d] = size(xTest);
[K,Da,Da1,Lab] = kernelMatrices(xTrain,xTest,kernelType,kernelParam);
MaTemp = permute(Da,[1,3,2]); D = reshape(MaTemp,[],n);
if nargout >=4
  MaTemp = permute(Lab,[1,2,4,3]); La = reshape(MaTemp,n,[],d);
end
if nargout >=6
%  MaTemp = permute(Lab,[1,3,2,4]); Lb = reshape(MaTemp,[],n,d);
  MaTemp = permute(La,[1,3,2]); L = reshape(MaTemp,[],n*d);
end
if nargout >=7
  F = [K D'];
  Za = cat(2,Da,La); ZaTemp = permute(Za,[1,3,2]); 
  Z = reshape(ZaTemp,[],n+n*d);
  zaNorm=0;
  for i=1:d
    zaNorm = max(zaNorm,norm(Za(:,:,i)));
  end
  Q = [K zeros(size(K,1),size(L,2)); 2*D L];
end
 
end
