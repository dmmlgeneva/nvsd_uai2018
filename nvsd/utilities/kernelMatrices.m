function [K,Da,Da1,Lab] = kernelMatrices(xTrain,xTest,kernelType,kernelParam)
% KERNELMATRICES - get kernal gram matrix and the kernel derivative matrices across train and test (or validation) instances
%   note: does not rescale the kernel matrices anyhow
%
% INPUTS
%   xTrain - train input maatrix n1 x d
%   xTest - test input matrix n2 x d (or train again)
%   kernelType - type kernel (pol,polHom,gauss,lin)
%   kernelParam - kernel parameter (specific for kernel type)
%
% OUTPUTS
%   K - n1 x n2 gram matrix with elements K(i,j) = kFun(xi,xj)
%   Da - n1 x n2 x d matrix of kernel derivatives with elements Da(i,j,a) = partial(kFun(xi,xj))/partial(xi_a)
%   Da1 - n2 x n1 x d matrix same as Da but xi is xTest and xj is xTrain
%   Lab - n1 x n2 x d x d matrix of kernel 2nd derivatives with elements Lab(i,j,a,b) = partial^2(kFun(xi,xj))/partial(xi_a)partial(xj_b)
%     
% EXAMPLE: K = getKernel(xTrain,xTest,'pol',2)
%
% CREATED: MG - 14/05/2017

%% choose the kernel function
switch kernelType
  case 'pol'
    kFunVec = @(xTr,xTe) polKernel(xTr,xTe,kernelParam,1);
  case 'polHom'
    kFunVec = @(xTr,xTe) polKernel(xTr,xTe,kernelParam,0);
  case 'gauss'
    kFunVec = @(xTr,xTe) gaussKernel(xTr,xTe,kernelParam);
  case 'gausn'
    kFunVec = @(xTr,xTe) gausnKernel(xTr,xTe,kernelParam);
  case 'lin'
    kFunVec = @(xTr,xTe) linKernel(xTr,xTe);
  otherwise
    error('unknown kernel')
end

%% get the matrices
if nargout==1
  [K] = kFunVec(xTrain,xTest);
elseif nargout==2
  [K,Da] = kFunVec(xTrain,xTest);
elseif nargout==3
  [K,Da,Da1] = kFunVec(xTrain,xTest);
else
  [K,Da,Da1,Lab] = kFunVec(xTrain,xTest);
end

%% polynomial kernel
function [k,da,da1,lab] = polKernel(xTr,xTe,p,c)
  d = size(xTe,2);
  linK = (xTr*xTe'+c);
  k = linK.^p;
  if nargout > 1
    dTemp = p*linK.^(p-1);
    dTemp1 = p*(xTe*xTr'+c).^(p-1);
    for a=1:d
      da(:,:,a) = bsxfun(@times,dTemp,[xTe(:,a)]');
      if nargout > 2
        da1(:,:,a) = bsxfun(@times,dTemp1,[xTr(:,a)]');
        if nargout > 3
          laTemp(:,:,a) = bsxfun(@times,p*(p-1)*linK.^(p-2),[xTe(:,a)]');
          for b=1:d
            lab(:,:,a,b) = bsxfun(@times,laTemp(:,:,a),xTr(:,b)) + logical(a==b)*dTemp;
          end
        end
      end
    end 
  end
end

%% gaussian: eq 1.27-1.29 in FutureWork2017March
function [k,da,da1,lab] = gaussKernel(xTr,xTe,p)
  [n,d] = size(xTr);
  n1 = size(xTe,1);
  rowsqTr = sum(xTr.^2,2);
  rowsqTe = sum(xTe.^2,2);
  prodX = 2*xTr*xTe';
  KTemp = bsxfun(@minus,rowsqTr,prodX);
  KTemp = bsxfun(@plus,KTemp,rowsqTe');
  k = exp(-KTemp/ (2*p^2));

  da = zeros(n,n1,d);
  da1 = zeros(n1,n,d);
  lab = zeros(n,n1,d,d);

  if nargout>1
    for a=1:d
      dTemp = bsxfun(@plus,-xTr(:,a),xTe(:,a)');
      da(:,:,a) = k.*dTemp/ (p^2);
      da1(:,:,a) = k'.*(-dTemp')/ (p^2);
      if nargout > 3
        for b=1:d
          lTemp = bsxfun(@plus,xTr(:,b),-xTe(:,b)');
          lab(:,:,a,b) = da(:,:,a).*lTemp / (p^2) + logical(a==b)*k/(p^2);
        end
      end
    end
  end
end

%% linear kernel
function [k,da,da1,lab] = linKernel(xTr,xTe)
  d = size(xTe,2);
  k = (xTr*xTe');
  if nargout>1
    for a=1:d
      da(:,:,a) = repmat([xTe(:,a)]',size(xTr,1),1);
      if nargout > 2
        da1(:,:,a) = repmat([xTr(:,a)]',size(xTe,1),1);
        if nargout > 2
          for b=1:d
            lab(:,:,a,b) = zeros(size(k)) + logical(a==b)*1;
          end
        end
      end
    end
  end
end

%% gaussian, normalized by number of dimensions
function [k,da,da1,lab] = gausnKernel(xTr,xTe,p)
  [n,d] = size(xTr);
  p = p*d; % normalization by the number of dimenstions (mean of chi-square is d)
  n1 = size(xTe,1);
  rowsqTr = sum(xTr.^2,2);
  rowsqTe = sum(xTe.^2,2);
  prodX = 2*xTr*xTe';
  KTemp = bsxfun(@minus,rowsqTr,prodX);
  KTemp = bsxfun(@plus,KTemp,rowsqTe');
  k = exp(-KTemp/ (2*p^2));

  da = zeros(n,n1,d);
  da1 = zeros(n1,n,d);
  lab = zeros(n,n1,d,d);

  if nargout>1
    for a=1:d
      dTemp = bsxfun(@plus,-xTr(:,a),xTe(:,a)');
      da(:,:,a) = k.*dTemp/ (p^2);
      da1(:,:,a) = k'.*(-dTemp')/ (p^2);
      if nargout > 3
        for b=1:d
          lTemp = bsxfun(@plus,xTr(:,b),-xTe(:,b)');
          lab(:,:,a,b) = da(:,:,a).*lTemp / (p^2) + logical(a==b)*k/(p^2);
        end
      end
    end
  end
end

end