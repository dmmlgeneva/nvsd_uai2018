function nvsdCalls(algo,expBase,resampleId,kFolds,kernelType,kernelParam,nu,mus,tau_max)
% NVSDCALLS - main calls to nvsd algorithms
%
% INPUTS
%   algo - name of algo ('lasso','enet','grplasso')
%   expBase - base name of experiment
%   resampleId - name of experiment
%   kFolds - number of cross validation folds
%   kernelType - type kernel (pol,polHom,gauss,lin)
%   kernelParam - kernel parameter (specific for kernel type)
%   mus - list of mu hyper-params for enet
%   tau_max - max value for tau grid
%
% EXAMPLE: mainCall('BH_',1)
%
% CREATED: MG 27/12/2017

% add folders to search path
addpath(genpath('./'));

%% fill in optional parameters
if ~exist('kernelParam','var') || isempty(kernelParam),
  kernelParam = [];
end
if ~exist('kfolds','var') || isempty(kfolds),
  kfolds = 5;
end
if ~exist('tau_max','var') || isempty(tau_max),
  if isdeployed
    tau_max = '';
  else
    tau_max = [];
  end
end
if ~exist('mus','var') || isempty(mus),
  if isdeployed
    mus = '';
  else
    mus = [];
  end
end

% if deployed covert string function arguments to numerics
if isdeployed
  cd ..;
  resampleId = str2num(resampleId); 
  kFolds = str2num(kFolds);
  kernelParam = str2num(kernelParam);
  nu = str2num(nu);
  mus = str2num(mus);
  tau_max = str2num(tau_max);
end

%% Experiments
expName = [expBase,num2str(resampleId,'%03d')];
switch algo
  case 'lasso'
    nvsd_lasso_main(expName,kFolds,kernelType,kernelParam,nu,tau_max)
  case 'enet'
    nvsd_enet_main(expName,kFolds,kernelType,kernelParam,nu,mus,tau_max)
  case 'grlasso'
    nvsd_grlasso_main(expName,kFolds,kernelType,kernelParam,nu,tau_max)
  otherwise
    fprintf('nvsdCalls: Unknown algo %s \n',algo);
end