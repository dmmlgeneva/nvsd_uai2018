function a = kr_ridge_regPath(K,y,lambdas)
% KR_RIDGE_REGPATH - kernel ridge regression solutions for multiple hyper-parameter values
%
% INPUTS
%   K - input kernel matrix
%   y - output vector
%   lambdas - vector of hyper-parameter values
%
% OUTPUTS
%   a - kernel ridge parameters, each column correspods to 1 lambda
%
% EXAMPLE: a = kr_ridge_regPath(K,y,lambdas,n)
%
% CREATED: MG 25/12/2017

d = size(K,2);
n = length(y);
ll = length(lambdas);
a = zeros(d,ll);
[V,D] = eig(K);
Vy = V'*y;
for i=1:ll
  a(:,i) = V*diag((diag(D)+n*lambdas(i)).^(-1))*Vy;
end

end
  
