function a = kr_ridge_simple(K,y,lambda,n)
% RIDGE_SIMPLE - ridge regression solution for single hyper-parameter value
%
% INPUTS
%   K - input kernel matrix
%   y - output vector
%   lambdas - vector of hyper-parameter values
%
% OUTPUTS
%   a - kernel ridge parameters, each column correspods to 1 lambda
%
% EXAMPLE: a = kr_ridge_simple(K,y,lambdas,n)
%
% CREATED: MG 25/12/2017

d = size(K,2);
n = length(y);
a = (K + n*lambda*eye(d))\y;

end
  
