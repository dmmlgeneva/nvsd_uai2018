function kr_ridge_main(expName,kFolds,kernelType,kernelParam)
% KR_RIDGE_MAIN - kernel ridge regression results
%
% INPUTS
%   expName - name of experiment
%   kfolds - integer number of cross validation samples (default=5)
%   kernelType - type kernel (pol,polHom,gauss,lin)
%   kernelParam - kernel parameter (specific for kernel type)
%
% OUTPUTS
%   saves kernel ridge results to Experiments/Results/expName
%
% EXAMPLE: kr_ridge_main('BH_001',5,'gauss')
%
% CREATED: MG 25/12/2017

tic
fprintf('Running kernel ridge regression for %s',expName);

%% fill in optional parameters
if ~exist('kfolds','var') || isempty(kfolds),
  kfolds = 5;
end

load(fullfile('Experiments/Data/',expName));
% train.X = train.X(:,[1,11]);
% valid.X = valid.X(:,[1,11]);
% test.X = test.X(:,[1,11]);

% train.X = train.X(:,[1:5,11:15]);
% valid.X = valid.X(:,[1:5,11:15]);
% test.X = test.X(:,[1:5,11:15]);

[n,d] = size(train.X);


%% fill in kernel Param
% guaussian width
if (strcmp(kernelType,'gauss') || strcmp(kernelType,'gausn')) && (~exist('kernelParam','var') || isempty(kernelParam)),
  kernelParam = gaussWidth(train.X,train.y);
% empty for linear kernel
elseif strcmp(kernelType,'lin')
  kernelParam = [];
end
if strcmp(kernelType,'gausn')
  kernelParam = kernelParam/d;
end

% 50-long lambda grid
sigma = normest(train.X'*train.X);
lambdas = sigma*(10.^(-9.8:.2:0));

% cross validation
%kr_rdg = kr_ridge_crossVal(train.X,train.y,lambdas,kFolds,kernelType,kernelParam,expName);

% hold-out validation
kr_rdg = kr_ridge_val(train,valid,lambdas,kernelType,kernelParam);
kr_rdg.lambdas = lambdas;

% test results
K = kernelMatrices(train.X,train.X,kernelType,kernelParam);
[kr_rdg.preds.train,kr_rdg.errs.train] = predict_Kr(K,train.y,kr_rdg.params.a);
K = kernelMatrices(train.X,test.X,kernelType,kernelParam);
[kr_rdg.preds.test,kr_rdg.errs.test] = predict_Kr(K,test.y,kr_rdg.params.a);

kr_rdg.kernelParam=kernelParam;
kr_rdg.elapsedTime=toc;

%% choose the kernel function and save results
switch kernelType
  case 'pol'
    kr_rdg_pol = kr_rdg;
    % save results
    resFile = fullfile('Experiments/Results',[expName,'.mat']);
    if exist(resFile, 'file') == 2
      save(resFile,'kr_rdg_pol','-append');
    else
      save(resFile,'kr_rdg_pol');
    end
  case 'polHom'
    kr_rdg_polHom = kr_rdg;
    % save results
    resFile = fullfile('Experiments/Results',[expName,'.mat']);
    if exist(resFile, 'file') == 2
      save(resFile,'kr_rdg_polHom','-append');
    else
      save(resFile,'kr_rdg_polHom');
    end
  case 'gauss'
    kr_rdg_gauss = kr_rdg;
    % save results
    resFile = fullfile('Experiments/Results',[expName,'.mat']);
    if exist(resFile, 'file') == 2
      save(resFile,'kr_rdg_gauss','-append');
    else
      save(resFile,'kr_rdg_gauss');
    end
  case 'gausn'
    kr_rdg_gausn = kr_rdg;
    % save results
    resFile = fullfile('Experiments/Results',[expName,'.mat']);
    if exist(resFile, 'file') == 2
      save(resFile,'kr_rdg_gausn','-append');
    else
      save(resFile,'kr_rdg_gausn');
    end
  case 'lin'
    kr_rdg_lin = kr_rdg;
    % save results
    resFile = fullfile('Experiments/Results',[expName,'.mat']);
    if exist(resFile, 'file') == 2
      save(resFile,'kr_rdg_lin','-append');
    else
      save(resFile,'kr_rdg_lin');
    end
  otherwise
    error('unknown kernel')
end


fprintf(', elapsed time = %f\n',toc);

end
