function [cvRes] = kr_ridge_val(train,valid,lambdas,kernelType,kernelParam)
% KR_RIDGE_VAL - train kernel ridge model and use validation sample to pick hyper-params
%
% INPUTS
%   train - struct with X and y fields
%   valid - struct with X and y fields
%   lambdas - vector of hyper-parameter values
%   kernelType - type kernel (pol,polHom,gauss,lin)
%   kernelParam - kernel parameter (specific for kernel type)
%
% OUTPUTS
%   cvResults - struct with fields
%     avgErrs.train - avg cv MSE for train data for all lambdas
%     avgErrs.test - avg cv MSE for test data for all lambdas
%     lbdOptIdx - index of lambda optimal in lambdas vector (minimising MSE over test)
%     lbdOpt - lambda optimal value
%     params.a - final model parameters
%
% EXAMPLE: cvRes = kr_ridge_corssVal(X,y,lambdas,5,'gauss',0.5)
%
% CREATED: MG 25/12/2017

ll = length(lambdas);

%% Train Models
% kernel matrix
K = kernelMatrices(train.X,train.X,kernelType,kernelParam);
a = kr_ridge_regPath(K,train.y,lambdas);
[~,cvErrs.train] = predict_Kr(K,train.y,a);
K = kernelMatrices(train.X,valid.X,kernelType,kernelParam);
[~,cvErrs.valid] = predict_Kr(K,valid.y,a);

cvRes.cvFolds.cvErrs = cvErrs;

%% get the avg errors and optimal lambda from avg cv test errors
cvRes.lbdOptIdx = find(cvErrs.valid==min(cvErrs.valid),1,'last');
cvRes.lbdOpt = lambdas(cvRes.lbdOptIdx);

%% final model
cvRes.params.a = a(:,cvRes.lbdOptIdx);


end


