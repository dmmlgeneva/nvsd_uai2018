function nvsd_enet_main(expName,kFolds,kernelType,kernelParam,nu,mus,tau_max,step1)
% NVSD_ENET_MAIN - nvsd enet results
%
% INPUTS
%   expName - name of experiment
%   kfolds - integer number of cross validation samples (default=5)
%   kernelType - type kernel (pol,polHom,gauss,lin)
%   kernelParam - kernel parameter (specific for kernel type)
%   nu - hilbert norm hyper-param
% INPUTS OPTIONAL
%   musCell - cell with mu list
%   tau_max - max value for tau grid
%   step1 - step1 learning (always finish full tau grid, otherwise stop when learning full models)
%
% OUTPUTS
%   saves nvsd enet results to Experiments/Results/expName
%
% EXAMPLE: nvsd_enet_main('BH_001',5,'gauss')
%
% CREATED: MG 27/12/2017

mainTic = tic;
fprintf('Running nvsd enet for %s\n',expName);

%% fill in optional parameters
if ~exist('nu','var') || isempty(nu),
  nu = 0;
end
if ~exist('mus','var') || isempty(mus)
  mus = [0.1;0.3;0.5;0.7;0.9]; % hyper-parameter weighting l1 and l2 combination (=1 uses only l1 as in lasso)
end
if ~exist('step1','var') || isempty(step1),
  step1 = 0;
end

load(fullfile('Experiments/Data/',expName));
[n,d] = size(train.X);

%% fill in kernel Param
% guaussian width
if (strcmp(kernelType,'gauss') || strcmp(kernelType,'gausn')) && (~exist('kernelParam','var') || isempty(kernelParam)),
  kernelParam = gaussWidth(train.X,train.y);
% empty for linear kernel
elseif strcmp(kernelType,'lin')
  kernelParam = [];
end
kPOrig = kernelParam;
if strcmp(kernelType,'gausn')
  kernelParam = kernelParam/d;
end
%% tau_max
if ~exist('tau_max','var') || isempty(tau_max),
  % train sample kernel matrices
  [~,~,~,~,~,~,F,Z,zaNorm] = nvsd_kernelMatrices(train.X,train.X,kernelType,kernelParam);
  tau_max = 1.1*zaNorm*norm(F\train.y)/sqrt(n);
end
% tau grid
taus = tauGrid(tau_max);
%taus = tauGrid(tau_max,[],5);


% add an extra cv step to select the best mu
for i = 1:length(mus)
  tausMu = taus/mus(i); % rescale tau so still has a chance to be sparse
  %nvsd_tmp = nvsd_enet_crossVal(train.X,train.y,tausMu,mus(i),kFolds,kernelType,kernelParam,expName);
  nvsd_tmp = nvsd_enet_val(train,valid,tausMu,mus(i),kernelType,kernelParam,nu,step1);
  kcv_1step(i) = nvsd_tmp.cvFolds.cvErrs.valid(nvsd_tmp.tauOptIdx);
  kcv_2steps(i) = nvsd_tmp.cvFolds.cvErrs.valid_step2(nvsd_tmp.tauOptIdx_step2,nvsd_tmp.lbdOptIdx_step2);
  nvsd_tmp.taus = tausMu; 
  nvsd_temp(i) = nvsd_tmp;
end
% select best smooth_par
muOptIdx = find(kcv_1step==min(kcv_1step),1);
muOptIdx_step2 = find(kcv_2steps==min(kcv_2steps),1);
% get the best step2 results
nvsd_enet = nvsd_temp(muOptIdx_step2);
nvsd_enet.taus_step2 = nvsd_temp(muOptIdx_step2).taus;
nvsd_enet.cvFolds_step2 = nvsd_temp(muOptIdx_step2).cvFolds;
% end correct entries with the best step 1 entries (watch out that you have all here!)
nvsd_enet.tauOptIdx = nvsd_temp(muOptIdx).tauOptIdx;
nvsd_enet.tauOpt = nvsd_temp(muOptIdx).tauOpt;
nvsd_enet.taus = nvsd_temp(muOptIdx).taus;
nvsd_enet.cvFolds = nvsd_temp(muOptIdx).cvFolds;
nvsd_enet.omg = nvsd_temp(muOptIdx).omg;
nvsd_enet.pdNorm = nvsd_temp(muOptIdx).pdNorm;
nvsd_enet.params.a = nvsd_temp(muOptIdx).params.a;
nvsd_enet.params.b = nvsd_temp(muOptIdx).params.b;
nvsd_enet.muOptIdx = muOptIdx;
nvsd_enet.muOptIdx_step2 = muOptIdx_step2;
nvsd_enet.muOpt = mus(muOptIdx);
nvsd_enet.muOpt_step2 = mus(muOptIdx_step2);

%% test results
% 1st step
[K,~,~,~,D] = nvsd_kernelMatrices(train.X,train.X,kernelType,kernelParam);
[nvsd_enet.preds.train,nvsd_enet.errs.train] = predict_Kr(K,train.y,nvsd_enet.params.a,D,nvsd_enet.params.b);
[K,~,~,~,D] = nvsd_kernelMatrices(train.X,test.X,kernelType,kernelParam);
[nvsd_enet.preds.test,nvsd_enet.errs.test] = predict_Kr(K,test.y,nvsd_enet.params.a,D,nvsd_enet.params.b);
% 2nd step
if sum(nvsd_enet.params.a_step2)==0
  % mean prediction for completely sparse model
  [nvsd_enet.preds.train_step2,nvsd_enet.errs.train_step2] = predict_Linear(train.X,train.y,zeros(size(train.X,2),1));
  [nvsd_enet.preds.test_step2,nvsd_enet.errs.test_step2] = predict_Linear(test.X,test.y,zeros(size(test.X,2),1));
else
  % kr_ridge model
  K = kernelMatrices(train.X(:,nvsd_enet.pdNorm_step2>0),train.X(:,nvsd_enet.pdNorm_step2>0),kernelType,kernelParam);
  [nvsd_enet.preds.train_step2,nvsd_enet.errs.train_step2] = predict_Kr(K,train.y,nvsd_enet.params.a_step2);
  K = kernelMatrices(train.X(:,nvsd_enet.pdNorm_step2>0),test.X(:,nvsd_enet.pdNorm_step2>0),kernelType,kernelParam);
  [nvsd_enet.preds.test_step2,nvsd_enet.errs.test_step2] = predict_Kr(K,test.y,nvsd_enet.params.a_step2);
end

nvsd_enet.kernelParam=kPOrig;
nvsd_enet.mus=mus;
nvsd_enet.nu=nu;
nvsd_enet.elapsedTime_cv=toc(mainTic);

%% choose the kernel function and save results
switch kernelType
  case 'pol'
    nvsd_enet_pol = nvsd_enet;
    % save results
    resFile = fullfile('Experiments/Results',[expName,'.mat']);
    if exist(resFile, 'file') == 2
      save(resFile,'nvsd_enet_pol','-append');
    else
      save(resFile,'nvsd_enet_pol');
    end
  case 'polHom'
    nvsd_enet_polHom = nvsd_enet;
    % save results
    resFile = fullfile('Experiments/Results',[expName,'.mat']);
    if exist(resFile, 'file') == 2
      save(resFile,'nvsd_enet_polHom','-append');
    else
      save(resFile,'nvsd_enet_polHom');
    end
  case 'gauss'
    nvsd_enet_gauss = nvsd_enet;
    % save results
    resFile = fullfile('Experiments/Results',[expName,'.mat']);
    if exist(resFile, 'file') == 2
      save(resFile,'nvsd_enet_gauss','-append');
    else
      save(resFile,'nvsd_enet_gauss');
    end
  case 'gausn'
    nvsd_enet_gausn = nvsd_enet;
    % save results
    resFile = fullfile('Experiments/Results',[expName,'.mat']);
    if exist(resFile, 'file') == 2
      save(resFile,'nvsd_enet_gausn','-append');
    else
      save(resFile,'nvsd_enet_gausn');
    end
  case 'lin'
    nvsd_enet_lin = nvsd_enet;
    % save results
    resFile = fullfile('Experiments/Results',[expName,'.mat']);
    if exist(resFile, 'file') == 2
      save(resFile,'nvsd_enet_lin','-append');
    else
      save(resFile,'nvsd_enet_lin');
    end
  otherwise
    error('unknown kernel')
end


fprintf('Total time = %f\n',toc(mainTic));

end