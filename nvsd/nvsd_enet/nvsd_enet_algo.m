function [omg,pdNorm,iter,admmTime] = nvsd_enet_algo(F,Z,Q,y,tau,mu,nu,initVal,stopCrit)
% NVSD_ENET_ALGO - nvsd enet algorithm
%
% INPUTS
%   F,Z,Q - kernel matrices
%   y - output vector
%   tau,mu - regularization hyper-parameter values
%   nu - hilbert norm hyper-param
%   initVal - initial values of optim variables
%   stopCrit - algorithm stopping criteria
%
% OUTPUTS
%   omg - nvsd lasso parameters
%   pdNorms - traning sample estimates of the partial derivative norms
%   iter - number of admm iterations
%   admmTime - admm elapsed time
%
% EXAMPLE: omg = nvsd_lasso_algo(m,y,taus)
%
% CREATED: MG 27/12/2017

n = length(y);
d = size(Z,1)/n;
dnFTF = F'*F*(2/n);
dnFTy = F'*y*(2/n);
ZTZ = Z'*Z;
nuQ = nu*Q;
nuQQT = nuQ+nuQ';


% ADMM total loss
lossTotal = @(om) norm(F*om-y)^2/n + sum( sqrt(sum( reshape(Z*om,n,[]).^2 )))*tau*mu/sqrt(n) + norm(Z*om)^2*tau*(1-mu)/n + om'*nuQ*om;
rprim = @(om,ph) norm(Z*om-ph); % primal residual
rdual = @(ph,phold,rh) norm(rh*Z'*(ph-phold)); % dual residual

%% initiate ADMM
omg = initVal.omg;
phi = initVal.phi;
lbd = zeros(size(phi));
% step size monitoring
rho = initVal.rho;
rhoTrace(1,1) = rho;
rhoUpdate = 1; % allow rho updating
% primal dual monitoring
resPrim(1,1) = rprim(omg,phi);
resDual(1,1) = rdual(phi,phi,rho);
% loss monitoring
lossEvalTotal(1,1) = lossTotal(omg);
lossMin=lossEvalTotal(1);
lossMinIter=1;

%% ADMM descent
iter = 1;
admmTic = tic;
while iter<stopCrit.maxIter
  iter = iter + 1;
  
  % step 1: steepest descent steps for omg (A is symmetric)
  A = dnFTF + ZTZ*(rho+tau*(1-mu)/n) + nuQQT;
  b = dnFTy + rho*(Z'*(phi-lbd));
  r = b-A*omg;
  for i=1:(1+floor(iter/20)) % progressively more iterations
    Ar = A*r; % crate for speed
    aStep = (r'*r)/(r'*Ar);
    omg = omg + aStep*r;
    r = r - aStep*Ar;
  end
  
  % step 2: proximal step for phi
  Zo = Z*omg; % crate for speed
  tr = tau*mu/ ( rho*sqrt(n));
  proxCenterMatrix = reshape(Zo+lbd,n,[]);
  proxCenterNorms = sqrt(sum(proxCenterMatrix.^2));
  proxThresholds = max(0, 1 - (tr*proxCenterNorms.^(-1)));
  phiMat = bsxfun(@times,proxCenterMatrix,proxThresholds);
  phiOld = phi; % need to keep for dual residual
  phi=phiMat(:);
  
  % step 3: update dual vars
  res = Zo - phi;
  lbd = lbd + res;
  
  % update ADMM monitoring
  resPrim(iter,1) = rprim(omg,phi);
  resDual(iter,1) = rdual(phi,phiOld,rho);
  lossEvalTotal(iter,1) = lossTotal(omg);
  if lossEvalTotal(iter,1)<lossMin
    lossMin = lossEvalTotal(iter,1);
    lossMinIter = iter;
  end
  
  % update rho (see Boyd2010 equation 3.13)
  if rhoUpdate
    % if loss increasing for a while fix rho for the remaining iterations
    if iter-lossMinIter>20
      rho = rhoTrace(lossMinIter);
      rhoUpdate = 0;
    elseif resPrim(iter) > 10*resDual(iter)
      rho = 2*rho; 
      lbd = lbd/2; % update the dual variable accrodingly! (see Boyd2010 end of the 3.13 section)
    elseif resDual(iter) > 10*resPrim(iter)
      rho = rho/2;
      lbd = lbd*2; % update the dual variable accrodingly! (see Boyd2010 end of the 3.13 section)
    end
  end
  rhoTrace(iter,1) = rho;
   
  % check convergence (see Boyd2017 3.12)
  epri = sqrt(n*d)*stopCrit.eabs + stopCrit.erel*max(norm(Zo),norm(phi));
  edual = sqrt(n+n*d)*stopCrit.eabs + stopCrit.erel*norm(Z'*lbd*rho);
  if resPrim(iter,1) < epri && resDual(iter,1) < edual
    break
  end

end

admmTime = toc(admmTic);
%% sample derivative norms
pdNorm = [sqrt(sum(reshape(phi,n,[]).^2))/sqrt(n)]';

end
  

