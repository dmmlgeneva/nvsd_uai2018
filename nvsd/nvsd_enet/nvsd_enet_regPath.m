function [omg,pdNorm,iter,admmTime] = nvsd_enet_regPath(F,Z,Q,y,taus,mu,nu,step1)
% NVSD_ENET_REGPATH - nvsd enet regression solutions for multiple tau hyper-parameter values (single mu)
%
% INPUTS
%   F,Z - kernel matrices
%   y - output vector
%   taus,mu - vector of tau hyper-parameter values
%   nu - hilbert norm hyper-param
%   step1 - step1 learning (always finish full tau grid, otherwise stop when learning full models)
%
% OUTPUTS
%   omg - nvsd lasso parameters, each column correspods to 1 tau
%   pdNorms - traning sample estimates of the partial derivative norms, each column correspods to 1 tau
%   iter - number of admm iterations
%   admmTime - admm elapsed time
%
% EXAMPLE: [omg,pdNorm,iter,admmTime] = nvsd_lasso_regPath(F,Z,y,taus,'gauss',0.5)
%
% CREATED: MG 27/12/2017

% stoping criteria
stopCrit.maxIter = 1000;
% abs and real factors for primal and dual ptimality
stopCrit.eabs = 1e-4;
stopCrit.erel=1e-4;
%stopCrit.eabs = 1e-6; % lower for exp6
%stopCrit.erel=1e-6; % lower for exp6
% initial parameter values
initVal.omg = pinv(F)*y;
initVal.phi = Z*initVal.omg;
initVal.rho = 0.1;

% comple regularization path
tt = length(taus);
n = size(F,1); d=(size(F,2)-n)/n;
omg = ones(size(F,2),tt);
pdNorm = ones(d,tt);
countFullpdNorms = 0;
for tIdx = 0:tt-1;
  tau = taus(length(taus)-tIdx);
  fprintf('nvsd enet: training tauIdx %d ... ',tIdx);
  [omg(:,tt-tIdx),pdNorm(:,tt-tIdx),iter(:,tt-tIdx),admmTime(:,tt-tIdx)] = nvsd_enet_algo(F,Z,Q,y,tau,mu,nu,initVal,stopCrit);
  initVal.omg=omg(:,tt-tIdx);
  initVal.phi = Z*initVal.omg;
  % how many times have all dimensions been selected
  if sum(logical(pdNorm(:,tt-tIdx)))==d;
    countFullpdNorms = countFullpdNorms+1;
  else 
    countFullpdNorms = 0;
  end
  fprintf('admm iterations %d, time %f \n',iter(:,tt-tIdx),admmTime(:,tt-tIdx));
  if ~step1 && countFullpdNorms==5
    fprintf('nvsd enet: full models so stop at iteration %d \n',tIdx);
    break
  end
end


end
  
