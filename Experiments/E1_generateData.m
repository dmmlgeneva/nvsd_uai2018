function E1_generateData(expId,trainSize,testSize)
% E1_GENERATEDATA - generate E1 synthetic datasets - explore group lasso
%
% INPUTS
%   expId - integer experiment Id
%   trainSize - integer size of training sample
%   testSize - integer size of test sample (validation will be the same size)
%
% EXAMPLE: E1_generateData(1,50,1000)
%
% CREATED: MG 3/1/2018

%% reproducible random data
rng(2018*expId*trainSize)

n = trainSize+2*testSize;

%% inputs
x = normrnd(0,1,n,18);
%% outputs
y = zeros(n,1);
for a=1:3
  for b=a:3
    for c=b:3
      y = y + x(:,a).*x(:,b).*x(:,c) - x(:,a+6).*x(:,b+6).*x(:,c+6);
    end
  end
end
y = y + normrnd(0,0.01,n,1);

%% split to train and test
permIdx = randperm(n);
train.X = x(permIdx(1:trainSize),:);
train.y = y(permIdx(1:trainSize));
valid.X = x(permIdx(trainSize+1:trainSize+testSize),:);
valid.y = y(permIdx(trainSize+1:trainSize+testSize));
test.X = x(permIdx(end-testSize+1:end),:);
test.y = y(permIdx(end-testSize+1:end));
% center outputs by train mean
train.y = train.y-mean(train.y);
valid.y = valid.y-mean(train.y);
test.y = test.y-mean(train.y);

% blocks of variables group lasso
d = size(train.X,2);
blocks = mat2cell(reshape(1:d,3,[]),3,ones(d/3,1));

%% save experimental data
save(['Experiments/Data/E1',num2str(trainSize,'%03d'),'_',num2str(expId,'%03d')],'train','test','valid','blocks')

end
