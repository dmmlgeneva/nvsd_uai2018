%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%% MG 4/1/2018                                              %%%%%
%%%% calls to functions to replicate synthetic E2 experiments %%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%% ISNTRUCTIONS %%%%%%%%%%%%%%%%%
%%%% Comment out the bit you want to run %%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% initiate
clear;
addpath(genpath('./'));

%% Generate experimental data
for trainSize = [30,50,70,90,110]
  for i=1:50
    E2_generateData(i,trainSize,1000)
  end
end

%% Fit base models
addpath(genpath('../PASPAL'));
kFolds = 1;
for trainSize = [30,50,70,90,110]
  for i=1:50
    expName = ['E2',num2str(trainSize,'%03d'),'_',num2str(i,'%03d')];
    mean_main(expName)
    ridge_main(expName,kFolds)
    lasso_main(expName,kFolds)
    grlasso_main(expName,kFolds)
    enet_main(expName,kFolds)
    kr_ridge_main(expName,kFolds,'pol',3)
  end
end

%% Other baslines
addpath(genpath('../DENOVAS_MG'));
addpath(genpath('../HSICLasso'));
kFolds = 1;
for trainSize = [30,50,70,90,110]
  for i=1:50
    expName = ['E2',num2str(trainSize,'%03d'),'_',num2str(i,'%03d')];
    denovas_main(expName,kFolds,'pol',3,10)
    HSIClasso_main(expName,'pol',3)
  end
end

%% NVSD models
% if you want to run these directly from here;
% watch out, takes time!!!!
% in reality, distributed as parallel jobs in slurm cluster (clusterCalls directory)
kFolds = 1;
for trainSize = [30,50,70,90,110]
  for i=1:50
    expName = ['E2',num2str(trainSize,'%03d'),'_',num2str(i,'%03d')];
    nvsd_lasso_main(expName,kFolds,'pol',3)
    nvsd_enet_main(expName,kFolds,'pol',3)
  end
end


%% summary results
clear;
addpath(genpath('./'));
% setup variables
trueSparsity = logical([1 1 1 0 0 0 1 1 1 0 0 0 0 0 0 0 0 0]);
fS = ones(18,1);
trainSizes = [30 50 70 90 110];
selMat=[]; rmseMat=[];
% get the summary results
for tS = 1:length(trainSizes)
  for rep=1:50
    expName = ['E2',num2str(trainSizes(tS),'%03d'),'_',num2str(rep,'%03d')];
    load(fullfile('Experiments/Results',expName));
    spam=load(fullfile('Experiments/Results/',['SPAM_',expName]));
    % mse values
    errs(rep,:) = [kr_rdg_pol.errs.test spam.errs_test HSIC_lss_pol.errs.test_step2 denovas_pol.errs.test_step2 ...
      nvsd_lss_pol.errs.test_step2 ... %nvsd_grlss_pol.errs.test_step2
      nvsd_enet_pol.errs.test_step2];
    % selected variables (complemented by all the full selection models)
    slct(:,:,rep) = [fS logical(spam.func_norm) HSIC_lss_pol.slct_step2 denovas_pol.model.selected_2steps ...
      logical(nvsd_lss_pol.pdNorm_step2) ... %logical(nvsd_grlss_pol.pdNorm_step2)
      logical(nvsd_enet_pol.pdNorm_step2)];
    % selection error
    for mIdx = 1:size(slct,2)
      selDist(rep,mIdx) = 1-jaccardIdx([trueSparsity',slct(:,mIdx,rep)]);
    end
  end
  % get averages
  RMSE = mean(sqrt(errs))';
  sRMSE = std(sqrt(errs))';
  SEL = mean(selDist)';
  sSEL = std(selDist)';
  selMat = [selMat SEL];
  rmseMat = [rmseMat RMSE];
  % wilcoxon sign-rank tests (1 means reject null hypothesis of median=0 at 5% significance level)
  for i=1:size(errs,2)
    [~,lssTestRMSE(tS,i)] = signrank(sqrt(errs(:,i)),sqrt(errs(:,end-1)));
    [~,enetTesRMSE(tS,i)] = signrank(sqrt(errs(:,i)),sqrt(errs(:,end)));
    [~,lssTestSEL(tS,i)] = signrank(selDist(:,i),selDist(:,end-1));
    [~,enetTestSEL(tS,i)] = signrank(selDist(:,i),selDist(:,end));
  end
end
% print into tex files to include into the paper
modelNames = {'Krls'  'SpAM' 'HSIC' 'Denovas ' 'NVSD(L)' 'NVSD(EN)'};
%tbFmt = '%0.2f (%0.2f)';
tbFmt = '%0.2f';
fID = fopen('Experiments/PrintOuts/E2_RMSE.tex','w');
fprintf(fID, ['%s & ', tbFmt,' & ', tbFmt,' & ', tbFmt,' & ', tbFmt,' & ', tbFmt,' \\\\ \n'],modelNames{1},rmseMat(1,:));
for mIdx=2:size(rmseMat,1)
  fprintf(fID, ['& & %s & ', tbFmt,' & ', tbFmt,' & ', tbFmt,' & ', tbFmt,' & ', tbFmt,' \\\\ \n'],modelNames{mIdx},rmseMat(mIdx,:));
end
fclose(fID);
fID = fopen('Experiments/PrintOuts/E2_SEL.tex','w');
fprintf(fID, ['%s & ', tbFmt,' & ', tbFmt,' & ', tbFmt,' & ', tbFmt,' & ', tbFmt,' \\\\ \n'],modelNames{1},selMat(1,:));
for mIdx=2:size(rmseMat,1)
  fprintf(fID, ['& & %s & ', tbFmt,' & ', tbFmt,' & ', tbFmt,' & ', tbFmt,' & ', tbFmt,' \\\\ \n'],modelNames{mIdx},selMat(mIdx,:));
end
fclose(fID);
