function E2_generateData(expId,trainSize,testSize)
% E2_GENERATEDATA - generate E1 synthetic datasets - explore enet
%
% INPUTS
%   expId - integer experiment Id
%   trainSize - integer size of training sample
%   testSize - integer size of test sample (validation will be the same size)
%
% EXAMPLE: E2_generateData(1,50,1000)
%
% CREATED: MG 4/1/2018

%% reproducible random data
rng(expId*trainSize)

n = trainSize+2*testSize;

%% inputs
% pair-wise correlation 0.95
R = [1 0.95; 0.95 1];
L = chol(R);
x = [];
for i=1:9
  x = [x normrnd(0,1,n,2)*L];
end
% re-arrange inputs
x = x(:,[1 3 5 7 9 11 2 4 6 8 10 12 13 14 15 16 17 18]);
%% outputs (same as Ex1)
y = zeros(n,1);
for a=1:3
  for b=1:3
    for c=1:3
      y = y + x(:,a).*x(:,b).*x(:,c) + x(:,a+6).*x(:,b+6).*x(:,c+6);
    end
  end
end
y = y + normrnd(0,0.01,n,1);

%% split to train and test
permIdx = randperm(n);
train.X = x(permIdx(1:trainSize),:);
train.y = y(permIdx(1:trainSize));
valid.X = x(permIdx(trainSize+1:trainSize+testSize),:);
valid.y = y(permIdx(trainSize+1:trainSize+testSize));
test.X = x(permIdx(end-testSize+1:end),:);
test.y = y(permIdx(end-testSize+1:end));
% center outputs by train mean
train.y = train.y-mean(train.y);
valid.y = valid.y-mean(train.y);
test.y = test.y-mean(train.y);

% blocks of variables for group lasso by the pairwise correlations
for i=1:6
  blocks{i} = [i;i+6];
end
blocks{7}=[13;14]; blocks{8}=[15;16]; blocks{9}=[17;18];

%% save experimental data
save(['Experiments/Data/E2',num2str(trainSize,'%03d'),'_',num2str(expId,'%03d')],'train','test','valid','blocks')

end
