function E3_generateData(expId,trainSize,testSize)
% E3_GENERATEDATA - generate E1 synthetic datasets - explore enet
%
% INPUTS
%   expId - integer experiment Id
%   trainSize - integer size of training sample
%   testSize - integer size of test sample (validation will be the same size)
%
% EXAMPLE: E3_generateData(1,50,1000)
%
% CREATED: MG 18/1/2018

%% reproducible random data
rng(expId*trainSize)

n = trainSize+2*testSize;

%% inputs
z = normrnd(0,1,n,6);
x = kron(z,ones(1,3)) + normrnd(0,0.1,n,3*6);
%% outputs (same as Ex1)
y = zeros(n,1);
y = 10*(z(:,1).^2 + z(:,3).^2).*exp(-2*(z(:,1).^2 + z(:,3).^2));
errs = normrnd(0,0.01,n,1);
y = y + errs;

%% split to train and test
permIdx = randperm(n);
train.X = x(permIdx(1:trainSize),:);
train.y = y(permIdx(1:trainSize));
train.Z = z(permIdx(1:trainSize),:);
train.errs = errs(permIdx(1:trainSize));
valid.X = x(permIdx(trainSize+1:trainSize+testSize),:);
valid.y = y(permIdx(trainSize+1:trainSize+testSize));
valid.Z = z(permIdx(trainSize+1:trainSize+testSize),:);
valid.errs = errs(permIdx(trainSize+1:trainSize+testSize));
test.X = x(permIdx(end-testSize+1:end),:);
test.y = y(permIdx(end-testSize+1:end));
test.Z = z(permIdx(end-testSize+1:end),:);
test.errs = errs(permIdx(end-testSize+1:end));
% center outputs by train mean
means.y = mean(train.y);
train.y = train.y-means.y;
valid.y = valid.y-means.y;
test.y = test.y-means.y;

% blocks of variables group lasso
d = size(train.X,2);
blocks = mat2cell(reshape(1:d,3,[]),3,ones(d/3,1));

%% save experimental data
save(['Experiments/Data/E3',num2str(trainSize,'%03d'),'_',num2str(expId,'%03d')],'train','test','valid','blocks','means')

end
