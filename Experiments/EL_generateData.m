function EL_generateData(expId,trainSize,testSize)
% EL_GENERATEDATA - generate F16 Elevators datasets
%
% INPUTS
%   expId - integer experiment Id
%   trainSize - integer size of training sample
%   testSize - integer size of test sample (validation will be the same size)
%
% EXAMPLE: EL_generateData(1,50,1000)
%
% CREATED: MG 15/2/2018

%% reproducible random data
rng(expId*trainSize)
n = trainSize+2*testSize;

%% load data
load('../../Data/LIACC/Elevators/elevators.mat')
% drop missing column
allData=elevators;
% drop end-2 column which is constant
allData(:,end-2)=[];
% center all data
allData = bsxfun(@minus,allData,mean(allData));
% normalize all data
allData(:,1:end) = bsxfun(@rdivide,allData(:,1:end),std(allData(:,1:end)));

%% inputs and outpus
x = allData(:,1:end-1);
y = allData(:,end);

%% split to train and test
permIdx = randperm(n);
train.X = x(permIdx(1:trainSize),:);
train.y = y(permIdx(1:trainSize));
valid.X = x(permIdx(trainSize+1:trainSize+testSize),:);
valid.y = y(permIdx(trainSize+1:trainSize+testSize));
test.X = x(permIdx(end-testSize+1:end),:);
test.y = y(permIdx(end-testSize+1:end));
% center outputs by train mean
train.y = train.y-mean(train.y);
valid.y = valid.y-mean(train.y);
test.y = test.y-mean(train.y);

% blocks of variables group lasso
blocks{1} = [1,6,8];
blocks{2} = [3,5,10];
blocks{3} = [2,13,17];
blocks{4} = [4,7,11,12];
blocks{5} = [9,14,15,16];

%% save experimental data
save(['Experiments/Data/EL',num2str(trainSize,'%03d'),'_',num2str(expId,'%03d')],'train','test','valid','blocks')

end