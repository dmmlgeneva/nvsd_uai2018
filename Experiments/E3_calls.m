%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%% MG 18/1/2018                                             %%%%%
%%%% calls to functions to replicate synthetic E3 experiments %%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%% ISNTRUCTIONS %%%%%%%%%%%%%%%%%
%%%% Comment out the bit you want to run %%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% initiate
clear;
addpath(genpath('./'));

%% Generate experimental data
for trainSize = [30,50,70,90,110]
  for rep=1:50
    E3_generateData(rep,trainSize,1000)
  end
end

%% Fit base models
addpath(genpath('../PASPAL'));
kFolds = 1;
for trainSize = [30,50,70,90,110]
  for rep=1:50
    expName = ['E3',num2str(trainSize,'%03d'),'_',num2str(rep,'%03d')];
    mean_main(expName)
    ridge_main(expName,kFolds)
    lasso_main(expName,kFolds)
    grlasso_main(expName,kFolds)
    enet_main(expName,kFolds)
    kr_ridge_main(expName,kFolds,'gauss',4)
  end
end

%% Other baslines (SPAM call in R!)
addpath(genpath('../DENOVAS_MG'));
addpath(genpath('../HSICLasso'));
kFolds = 1;
for trainSize = [30,50,70,90,110]
  for rep=1:50
    expName = ['E3',num2str(trainSize,'%03d'),'_',num2str(rep,'%03d')];
    denovas_main(expName,kFolds,'gauss',4,10)
    HSIClasso_main(expName,'gauss',4)
  end
end

%% NVSD models
% if you want to run these directly from here;
% watch out, takes time!!!!
% in reality, distributed as parallel jobs in slurm cluster (clusterCalls directory)
kFolds = 1;
for trainSize = [30,50,70,90,110]
  for rep=1:50
    expName = ['E3',num2str(trainSize,'%03d'),'_',num2str(rep,'%03d')];
    nvsd_lasso_main(expName,kFolds,'gausn',4,0.5)
    nvsd_grlasso_main(expName,kFolds,'gausn',4,0.5)
    nvsd_enet_main(expName,kFolds,'gausn',4,0.5)
  end
end


%% summary results
clear;
addpath(genpath('./'));
% setup variables
trueSparsity = logical([1 1 1 0 0 0 1 1 1 0 0 0 0 0 0 0 0 0]);
fS = ones(18,1);
trainSizes = [30 50 70 90 110];
selMat=[]; rmseMat=[];
% get the summary results
for tS = 1:length(trainSizes)
  for rep=1:50
    expName = ['E3',num2str(trainSizes(tS),'%03d'),'_',num2str(rep,'%03d')];
    load(fullfile('Experiments/Results',expName));
    spam=load(fullfile('Experiments/Results/',['SPAM_',expName]));
    % mse values
    errs(rep,:) = [kr_rdg_gauss.errs.test spam.errs_test ...
      HSIC_lss_gauss.errs.test_step2 ...
      denovas_gauss.errs.test_step2 ...
      nvsd_lss_gausn.errs.test_step2  nvsd_grlss_gausn.errs.test_step2 nvsd_enet_gausn.errs.test_step2];
    % selected variables (complemented by all the full selection models)
    slct(:,:,rep) = [fS logical(spam.func_norm) HSIC_lss_gauss.slct_step2 ...
      denovas_gauss.model.selected_2steps ...
      logical(nvsd_lss_gausn.pdNorm_step2) logical(nvsd_grlss_gausn.pdNorm_step2) logical(nvsd_enet_gausn.pdNorm_step2)];   
    % selection error
    for mIdx = 1:size(slct,2)
      selDist(rep,mIdx) = 1-jaccardIdx([trueSparsity',slct(:,mIdx,rep)]);
    end
  end
  % stability
  for mIdx = 1:size(slct,2)
    selStab(mIdx,tS) = jaccardIdx(squeeze(slct(:,mIdx,:)));
  end
  % get averages
  RMSE = mean(sqrt(errs))';
  sRMSE = std(sqrt(errs))';
  SEL = mean(selDist)';
  sSEL = std(selDist)';
  selMat = [selMat SEL];
  rmseMat = [rmseMat RMSE];
  % wilcoxon sign-rank tests (1 means reject null hypothesis of median=0 at 5% significance level)
  for rep=1:size(errs,2)
    [~,lssTestRMSE(tS,rep)] = signrank(sqrt(errs(:,rep)),sqrt(errs(:,end-2)));
    [~,grlssTesRMSE(tS,rep)] = signrank(sqrt(errs(:,rep)),sqrt(errs(:,end-1)));
    [~,enetTesRMSE(tS,rep)] = signrank(sqrt(errs(:,rep)),sqrt(errs(:,end)));
    [~,lssTestSEL(tS,rep)] = signrank(selDist(:,rep),selDist(:,end-2));
    [~,grlssTestSEL(tS,rep)] = signrank(selDist(:,rep),selDist(:,end-1));
    [~,enetTestSEL(tS,rep)] = signrank(selDist(:,rep),selDist(:,end));
  end
end
% print into tex files to include into the paper
modelNames = {'Krls'  'SpAM' 'HSIC' 'Denovas ' 'NVSD(L)' 'NVSD(GL)' 'NVSD(EN)'};
%tbFmt = '%0.2f (%0.2f)';
tbFmt = '%0.2f';
fID = fopen('Experiments/PrintOuts/E3_RMSE.tex','w');
fprintf(fID, ['%s & ', tbFmt,' & ', tbFmt,' & ', tbFmt,' & ', tbFmt,' & ', tbFmt,' \\\\ \n'],modelNames{1},rmseMat(1,:));
for mIdx=2:size(rmseMat,1)
  fprintf(fID, ['& & %s & ', tbFmt,' & ', tbFmt,' & ', tbFmt,' & ', tbFmt,' & ', tbFmt,' \\\\ \n'],modelNames{mIdx},rmseMat(mIdx,:));
end
fclose(fID);
fID = fopen('Experiments/PrintOuts/E3_SEL.tex','w');
fprintf(fID, ['%s & ', tbFmt,' & ', tbFmt,' & ', tbFmt,' & ', tbFmt,' & ', tbFmt,' \\\\ \n'],modelNames{1},selMat(1,:));
for mIdx=2:size(rmseMat,1)
  fprintf(fID, ['& & %s & ', tbFmt,' & ', tbFmt,' & ', tbFmt,' & ', tbFmt,' & ', tbFmt,' \\\\ \n'],modelNames{mIdx},selMat(mIdx,:));
end
% print stability
fID = fopen('Experiments/PrintOuts/E3_Stability.tex','w');
fprintf(fID, ['%s & %0.2f & %0.2f & %0.2f & %0.2f & %0.2f \\\\ \n'],modelNames{1},selStab(1,:));
for mIdx=2:size(rmseMat,1)
  fprintf(fID, ['& & %s & %0.2f & %0.2f & %0.2f & %0.2f & %0.2f \\\\ \n'],modelNames{mIdx},selStab(mIdx,:));
end
fclose(fID);


%plotting
rep=17
load(['Experiments/Results/E3110_',num2str(rep,'%03d'),'.mat'])
spam=load(['Experiments/Results/SPAM_E3110_',num2str(rep,'%03d'),'.mat']);
load(['Experiments/Data/E3110_',num2str(rep,'%03d'),'.mat'])
figure
hs(1) = subplot(4,2,1);
hs(2) = subplot(4,2,2);
hs(3) = subplot(4,2,3);
hs(4) = subplot(4,2,4);
hs(5) = subplot(4,2,5);
hs(6) = subplot(4,2,6);
hs(7) = subplot(4,2,7);
hs(8) = subplot(4,2,8);
scatter3(hs(1),test.Z(:,1),test.Z(:,3),test.y,4,test.y)
scatter3(hs(2),test.Z(:,1),test.Z(:,3),kr_rdg_gauss.preds.test,4,kr_rdg_gauss.preds.test)
scatter3(hs(3),test.Z(:,1),test.Z(:,3),spam.preds_test,4,spam.preds_test)
scatter3(hs(4),test.Z(:,1),test.Z(:,3),HSIC_lss_gauss.preds.test_step2,4,HSIC_lss_gauss.preds.test_step2)
scatter3(hs(5),test.Z(:,1),test.Z(:,3),denovas_gauss.preds.test_step2,4,denovas_gauss.preds.test_step2)
scatter3(hs(6),test.Z(:,1),test.Z(:,3),nvsd_lss_gausn.preds.test_step2,4,nvsd_lss_gausn.preds.test_step2)
scatter3(hs(7),test.Z(:,1),test.Z(:,3),nvsd_grlss_gausn.preds.test_step2,4,nvsd_grlss_gausn.preds.test_step2)
scatter3(hs(8),test.Z(:,1),test.Z(:,3),nvsd_enet_gausn.preds.test_step2,4,nvsd_enet_gausn.preds.test_step2)
for i=1:8
  xlim(hs(i),[-3 3]);
  ylim(hs(i),[-3 3]);
  zlim(hs(i),[-2 2]);
  view(hs(i),[-30 15]);
  xlabel(hs(i),'$z_1$','Interpreter','latex');
  ylabel(hs(i),'$z_3$','Interpreter','latex');
  zlabel(hs(i),'$y$','Interpreter','latex');
end
title(hs(1),'True')
title(hs(2),'Krls')
title(hs(3),'SpAM')
title(hs(4),'HSIC')
title(hs(5),'Denovas')
title(hs(6),'NVSD(L)')
title(hs(7),'NVSD(GL)')
title(hs(8),'NVSD(EN)')







