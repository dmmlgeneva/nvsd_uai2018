%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%% MG 13/2/2018                                                  %%%%%
%%%% calls to functions to replicate Energy Efficiency experiments %%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%% ISNTRUCTIONS %%%%%%%%%%%%%%%%%
%%%% Comment out the bit you want to run %%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% initiate
clear;
addpath(genpath('./'));

%% Generate experimental data
for trainSize = [100]
  for rep=1:50
    ENGL_generateData(rep,trainSize,300)
  end
end

%% Fit base models
% addpath(genpath('../PASPAL'));
kFolds = 1;
for trainSize = [100]
  for rep=1:50
    expName = ['ENGL',num2str(trainSize,'%03d'),'_',num2str(rep,'%03d')];
    mean_main(expName)
    ridge_main(expName,kFolds)
    lasso_main(expName,kFolds)
    grlasso_main(expName,kFolds)
    enet_main(expName,kFolds)
    kr_ridge_main(expName,kFolds,'pol',3)
  end
end

%% Other baslines
addpath(genpath('../DENOVAS_MG'));
addpath(genpath('../HSICLasso'));
kFolds = 1;
for trainSize = [100]
  for i=1:50
    expName = ['ENGL',num2str(trainSize,'%03d'),'_',num2str(i,'%03d')];
    denovas_main(expName,kFolds,'pol',3,10)
    HSIClasso_main(expName,'pol',3)
  end
end

%% NVSD models
% if you want to run these directly from here;
% watch out, takes time!!!!
% in reality, distributed the for loops as parallel jobs in slurm cluster (clusterCalls directory)
kFolds = 1;
for trainSize = [100]
  for rep=1:50
    expName = ['ENGL',num2str(trainSize,'%03d'),'_',num2str(rep,'%03d')];
    nvsd_lasso_main(expName,kFolds,'pol',3)
    nvsd_enet_main(expName,kFolds,'pol',3)
  end
end


%% summary results
clear;
addpath(genpath('./'));
% setup variables
trainSizes = [100];
% get the summary results
for tS = 1:length(trainSizes)
  for rep=1:50
    expName = ['ENGL',num2str(trainSizes(tS),'%03d'),'_',num2str(rep,'%03d')];
    load(fullfile('Experiments/Results',expName));
    % mse values
    errs(rep,:) = [kr_rdg_pol.errs.test HSIC_lss_pol.errs.test_step2 denovas_pol.errs.test_step2];
%      nvsd_lss_pol.errs.test_step2 nvsd_enet_pol.errs.test_step2 ...
%      nvsd_lss_pol.errs.test_step2 nvsd_enet_pol.errs.test_step2];
  end
  % get averages
  %errs(1:40,:)=[];
  RMSE(tS,:) = mean(sqrt(errs))';
  sRMSE(tS,:) = std(sqrt(errs))';
end
RMSE

%% summary results
clear;
addpath(genpath('./'));
% setup variables
fS = ones(8,1);
trainSizes = [100];
selMat=[]; rmseMat=[];
% get the summary results
for tS = 1:length(trainSizes)
  for rep=1:50
    expName = ['ENGL',num2str(trainSizes(tS),'%03d'),'_',num2str(rep,'%03d')];
    load(fullfile('Experiments/Results',expName));
    %spam=load(fullfile('Experiments/Results/',['SPAM_',expName]));
    % mse values
    errs(rep,:) = [kr_rdg_pol.errs.test HSIC_lss_pol.errs.test_step2 denovas_pol.errs.test_step2 ... 
      nvsd_lss_pol.errs.test_step2 nvsd_enet_pol.errs.test_step2];
    % selected variables (complemented by all the full selection models)
    %spam.errs_test 
    slct(:,:,rep) = [fS HSIC_lss_pol.slct_step2 denovas_pol.model.selected_2steps ...
      logical(nvsd_lss_pol.pdNorm_step2) logical(nvsd_enet_pol.pdNorm_step2)];
    %logical(spam.func_norm) 
    % precistion / recall / size of support
    for mIdx = 1:size(slct,2)
      selCount(rep,mIdx) = sum(slct(:,mIdx,rep));
    end
  end
  % stability
  for mIdx = 1:size(slct,2)
    selStab(mIdx,tS) = jaccardIdx(squeeze(slct(:,mIdx,:)));
  end
  % get averages
  RMSE = mean(sqrt(errs))';
  sRMSE = std(sqrt(errs))';
  SEL = mean(selCount)';
  sSEL = std(selCount)';
  selMat = [selMat SEL];
  rmseMat = [rmseMat RMSE];
  % wilcoxon sign-rank tests (1 means reject null hypothesis of median=0 at 5% significance level)
  for i=1:size(errs,2)
    [~,lssTestRMSE(tS,i)] = signrank(sqrt(errs(:,i)),sqrt(errs(:,end-1)));
    [~,enetTesRMSE(tS,i)] = signrank(sqrt(errs(:,i)),sqrt(errs(:,end)));
    [~,lssTestSEL(tS,i)] = signrank(selCount(:,i),selCount(:,end-1));
    [~,enetTestSEL(tS,i)] = signrank(selCount(:,i),selCount(:,end));
  end
end
rmseMat
selMat

