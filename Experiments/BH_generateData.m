function BH_generateData(expId,trainSize,testSize)
% BH_GENERATEDATA - generate BostonHousing datasets
%
% INPUTS
%   expId - integer experiment Id
%   trainSize - integer size of training sample
%   testSize - integer size of test sample (validation will be the same size)
%
% EXAMPLE: BH_generateData(1,50,1000)
%
% CREATED: MG 3/1/2018

%% reproducible random data
rng(expId*trainSize)
n = trainSize+2*testSize;

%% load data
load('../../Data/UCI/BostonHousing/housing.mat')

% center all data
housing = bsxfun(@minus,housing,mean(housing));
% normalize inputs
housing(:,1:end-1) = bsxfun(@rdivide,housing(:,1:end-1),std(housing(:,1:end-1)));

%% inputs and outpus
% drop categorical CHAS and RAD variables and mostly zero ZN variable
x = housing(:,[1,3,5,6,7,8,10,11,12,13]);
y = housing(:,end);

%% split to train and test
permIdx = randperm(n);
train.X = x(permIdx(1:trainSize),:);
train.y = y(permIdx(1:trainSize));
valid.X = x(permIdx(trainSize+1:trainSize+testSize),:);
valid.y = y(permIdx(trainSize+1:trainSize+testSize));
test.X = x(permIdx(end-testSize+1:end),:);
test.y = y(permIdx(end-testSize+1:end));
% center outputs by train mean
train.y = train.y-mean(train.y);
valid.y = valid.y-mean(train.y);
test.y = test.y-mean(train.y);

% blocks of variables group lasso
d = size(train.X,2);
gsize = 1;
blocks = mat2cell(reshape(1:d,gsize,[]),gsize,ones(d/gsize,1));

%% save experimental data
save(['Experiments/Data/BH',num2str(trainSize,'%03d'),'_',num2str(expId,'%03d')],'train','test','valid','blocks')

end