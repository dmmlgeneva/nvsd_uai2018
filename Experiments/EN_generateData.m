function EN_generateData(expId,trainSize,testSize)
% EN_GENERATEDATA - generate Energy Efficiency datasets
%
% INPUTS
%   expId - integer experiment Id
%   trainSize - integer size of training sample
%   testSize - integer size of test sample (validation will be the same size)
%
% EXAMPLE: CP_generateData(1,50,1000)
%
% CREATED: MG 13/2/2018

%% reproducible random data
rng(expId*trainSize)
n = trainSize+2*testSize;

%% load data
load('../../Data/UCI/EnergyEfficiency/ENB2012_data.mat')
allData=ENB2012data;
% center all data
allData = bsxfun(@minus,allData,mean(allData));
% normalize inputs
allData(:,1:end-2) = bsxfun(@rdivide,allData(:,1:end-2),std(allData(:,1:end-2)));

%% inputs and outpus
x = allData(:,1:end-2);
% has two possible output columns
y = allData(:,end-1);

%% split to train and test
permIdx = randperm(n);
train.X = x(permIdx(1:trainSize),:);
train.y = y(permIdx(1:trainSize));
valid.X = x(permIdx(trainSize+1:trainSize+testSize),:);
valid.y = y(permIdx(trainSize+1:trainSize+testSize));
test.X = x(permIdx(end-testSize+1:end),:);
test.y = y(permIdx(end-testSize+1:end));
% center outputs by train mean
train.y = train.y-mean(train.y);
valid.y = valid.y-mean(train.y);
test.y = test.y-mean(train.y);

% blocks of variables group lasso
d = size(train.X,2);
gsize = 1;
blocks = mat2cell(reshape(1:d,gsize,[]),gsize,ones(d/gsize,1));

%% save experimental data
save(['Experiments/Data/EN',num2str(trainSize,'%03d'),'_',num2str(expId,'%03d')],'train','test','valid','blocks')

end