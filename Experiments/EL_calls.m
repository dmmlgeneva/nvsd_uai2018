%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%% MG 15/2/2018                                              %%%%%
%%%% calls to functions to replicate F16 Elevators experiments %%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%% ISNTRUCTIONS %%%%%%%%%%%%%%%%%
%%%% Comment out the bit you want to run %%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% initiate
clear;
addpath(genpath('./'));

%% Generate experimental data
for trainSize = [100]
  for rep=1:50
    EL_generateData(rep,trainSize,1000)
  end
end

%% Fit base models
addpath(genpath('../PASPAL'));
kFolds = 1;
for trainSize = [100]
  for rep=1:50
    expName = ['EL',num2str(trainSize,'%03d'),'_',num2str(rep,'%03d')];
    kr_ridge_main(expName,kFolds,'gauss',5)
    mean_main(expName)
    ridge_main(expName,kFolds)
    lasso_main(expName,kFolds)
    grlasso_main(expName,kFolds)
    enet_main(expName,kFolds)
    kr_ridge_main(expName,kFolds,'pol',3)
  end
end

%% Other baslines
addpath(genpath('../DENOVAS_MG'));
addpath(genpath('../HSICLasso'));
kFolds = 1;
for trainSize = [100]
  for i=1:50
    expName = ['EL',num2str(trainSize,'%03d'),'_',num2str(i,'%03d')];
    denovas_main(expName,kFolds,'gauss',5,10)
    HSIClasso_main(expName,'gauss',5)
  end
end

%% NVSD models
% if you want to run these directly from here;
% watch out, takes time!!!!
% in reality, distributed the for loops as parallel jobs in slurm cluster (clusterCalls directory)
kFolds = 1;
for trainSize = [100]
  for rep=1:50
    expName = ['EL',num2str(trainSize,'%03d'),'_',num2str(rep,'%03d')];
    nvsd_lasso_main(expName,kFolds,'gauss',5,10)
    nvsd_enet_main(expName,kFolds,'gauss',5,10)
    nvsd_grlss_main(expName,kFolds,'gauss',5,10)
  end
end

%% summary results
clear;
addpath(genpath('./'));
% setup variables
fS = ones(17,1);
trainSizes = [100];
selMat=[]; rmseMat=[];
% get the summary results
for tS = 1:length(trainSizes)
  for rep=1:50
    expName = ['EL',num2str(trainSizes(tS),'%03d'),'_',num2str(rep,'%03d')];
    load(fullfile('Experiments/Results',expName));
    %spam=load(fullfile('Experiments/Results/',['SPAM_',expName]));
    % mse values
    errs(rep,:) = [kr_rdg_gauss.errs.test ...
      HSIC_lss_gauss.errs.test_step2 denovas_gauss.errs.test_step2 ...
      nvsd_lss_gauss.errs.test_step2 nvsd_enet_gauss.errs.test_step2 nvsd_grlss_gauss.errs.test_step2];
    % selected variables (complemented by all the full selection models)
    %spam.errs_test 
    slct(:,:,rep) = [fS HSIC_lss_gauss.slct_step2 denovas_gauss.model.selected_2steps ...
      logical(nvsd_lss_gauss.pdNorm_step2) logical(nvsd_enet_gauss.pdNorm_step2) logical(nvsd_grlss_gauss.pdNorm_step2)];
    %logical(spam.func_norm) 
    % precistion / recall / size of support
    for mIdx = 1:size(slct,2)
      selCount(rep,mIdx) = sum(slct(:,mIdx,rep));
    end
  end
  % stability
  for mIdx = 1:size(slct,2)
    selStab(mIdx,tS) = jaccardIdx(squeeze(slct(:,mIdx,:)));
  end
  % get averages
  %errs(1:30,:)=[]; slct(:,:,1:30)=[];
  RMSE = mean(sqrt(errs))';
  sRMSE = std(sqrt(errs))';
  SEL = mean(selCount)';
  sSEL = std(selCount)';
  selMat = [selMat SEL];
  rmseMat = [rmseMat RMSE];
  % wilcoxon sign-rank tests (1 means reject null hypothesis of median=0 at 5% significance level)
  for i=1:size(errs,2)
    [~,lssTestRMSE(tS,i)] = signrank(sqrt(errs(:,i)),sqrt(errs(:,end-2)));
    [~,enetTesRMSE(tS,i)] = signrank(sqrt(errs(:,i)),sqrt(errs(:,end-1)));
    [~,grlssTesRMSE(tS,i)] = signrank(sqrt(errs(:,i)),sqrt(errs(:,end)));
    [~,lssTestSEL(tS,i)] = signrank(selCount(:,i),selCount(:,end-2));
    [~,enetTestSEL(tS,i)] = signrank(selCount(:,i),selCount(:,end-1));
    [~,grlssTestSEL(tS,i)] = signrank(selCount(:,i),selCount(:,end));
  end
end
10*rmseMat
selMat

