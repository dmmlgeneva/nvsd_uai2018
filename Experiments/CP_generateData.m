function CP_generateData(expId,trainSize,testSize)
% CP_GENERATEDATA - generate Computer Activity datasets
%
% INPUTS
%   expId - integer experiment Id
%   trainSize - integer size of training sample
%   testSize - integer size of test sample (validation will be the same size)
%
% EXAMPLE: CP_generateData(1,50,1000)
%
% CREATED: MG 23/1/2018

%% reproducible random data
rng(expId*trainSize)
n = trainSize+2*testSize;

%% load data
load('../../Data/LIACC/ComputerActivity/cpuact.mat')
allData=cpuact;
% center all data
allData = bsxfun(@minus,allData,mean(allData));
% normalize inputs
allData(:,1:end-1) = bsxfun(@rdivide,allData(:,1:end-1),std(allData(:,1:end-1)));

%% inputs and outpus
x = allData(:,1:end-1);
y = allData(:,end);

%% split to train and test
permIdx = randperm(n);
train.X = x(permIdx(1:trainSize),:);
train.y = y(permIdx(1:trainSize));
valid.X = x(permIdx(trainSize+1:trainSize+testSize),:);
valid.y = y(permIdx(trainSize+1:trainSize+testSize));
test.X = x(permIdx(end-testSize+1:end),:);
test.y = y(permIdx(end-testSize+1:end));
% center outputs by train mean
train.y = train.y-mean(train.y);
valid.y = valid.y-mean(train.y);
test.y = test.y-mean(train.y);

% blocks of variables group lasso
blocks{1} = [3,17,18,19,21];
blocks{2} = [1,2,4,5,6];
blocks{3} = [7,8,9,10,11];
blocks{4} = [12,13,14,15,16,20];

%% save experimental data
save(['Experiments/Data/CP',num2str(trainSize,'%03d'),'_',num2str(expId,'%03d')],'train','test','valid','blocks')

end