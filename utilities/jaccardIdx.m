function tDist = jaccardIdx(selMatrix)
% JACCARDIDX - average Jaccard index (1-tanimoto distance) for a matrix with feature selection vectors
%   using measure from Kalousis2007: Stability of feature selection algorithms: a study on high-dimensional spaces
%
% INPUTS
%   selMatrix - zero-one matrix, number of rows = number of features, each column is a selection vector
%
% OUTPUTS
%   tDist - average tanimoto distance
%
% EXAMPLE: tDist = jaccardIdx([1 0 1 1 1; 1 1 1 0 1; 0 1 1 0 1; 1 1 0 0 0])
%
% CREATED: MG 7/1/2018

selMatrix = logical(selMatrix);
n = size(selMatrix,2);
td = 0;
% get all pairwise distances
for i=1:n-1
  for j=i+1:n
    td = td + min(1,sum(and(selMatrix(:,i),selMatrix(:,j)))/sum(or(selMatrix(:,i),selMatrix(:,j)))); % min to avoid nans
  end
end

tDist = td/(n*(n-1)/2);

end