function [cvFolds] = splitCV(X,y,kfolds,expName)
% SPLITCV - split data into kfolds cross validation samples
%
% INPUTS
%   X - input data matrix
%   y - output vector
%   kfolds - integer number of cross validation samples (default=5)
% INPUTS OPTIONAL
%   expName - experiment name to load validation data from
%
% OUTPUTS
%   w - ridge parameters, each column correspods to 1 lambda
%
% EXAMPLE: [cvFolds] = splitCV(train.X,train.y,5)
%
% CREATED: MG 25/12/2017

n = size(X,1);
rng(n); % reproducible cv splits

if kfolds<2
% use validation sample
  cvFolds(1).train.X = X;
  cvFolds(1).train.y = y;
  tmp = load(fullfile('Experiments/Data/',expName),'valid');
  cvFolds(1).test = tmp.valid;
else
% standard inner kfold crossvalidation
  % get cv indexes
  idx = crossvalind('Kfold',n,kfolds);
  % create cv samples and center (to avoid offset problems)
  for i=1:kfolds
    xTrainTemp = X(idx~=i,:);
    yTrainTemp = y(idx~=i,:);
    cvFolds(i).train.X = bsxfun(@minus,xTrainTemp,mean(xTrainTemp));
    cvFolds(i).train.y = yTrainTemp - mean(yTrainTemp);
    cvFolds(i).test.X = bsxfun(@minus,X(idx==i,:),mean(xTrainTemp));
    cvFolds(i).test.y = y(idx==i,:) - mean(yTrainTemp);
  end
end