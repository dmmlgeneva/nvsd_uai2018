# NVSD_UAI2018
MG 21/2/2018

Code and replication files for NVSD_UAI2018 paper Structured nonlinear variable selection

Folders:
- utilities - some general utility faunctions  
- nvsd - main folder for the nvsd methods  
- linearModels - calls to baseline linear models  
- Experiments - code to replicate the paper experiments  
- clusterDeploy - mcc compile call  
- clusterCalls - bash scripts for deployment over cluster  
- baselineModels - calls to baseline models  
- DENOVAS_MG - corrected original denovas code  



