function [cv_output,model] = nvs_kcv(X,Y,varargin)
%NVS_KCV Parameters choice through K-fold or LOO cross validation for 
%the l1l2-algorithm (used as variables selector) followed by Regularized Least
%Squares (used as estimator).
%   CV_OUTPUT = NVS_KCV(X,Y) Given training set, (X,Y), performs leave-one-out cross
%   validation for NVS algorithm. NVS is used for selection of the variables,
%   whereas the regression coefficient are evaluated on the selected variables 
%   via RLS. The input data X is a NxD matrix, and the labels Y are a Nx1
%   vector
%   [CV_OUTPUT,MODEL] = NVS_KCV(X,Y) Also returns the estimated model
% 
%   NVS_KCV(...,'PropertyName',PropertyValue,...) sets properties to the
%   specified property values.
%       -'kernel': kernel type, 'linear','pol'(default),'gaussian
%       -'kernel_par': parameter for the kernel, degree for polynomial kernel
%        (default is 2), sigma for gaussian kernel
%       -'NVS_pars': vector of values for the NVS parameter. If not given,
%        100 values are chosen automatically.
%       -'NVS_n_par': number of values for the NVS parameter.
%       -'NVS_max_par': maximum value for the NVS parameter
%       -'NVS_min_par': minimum value for the NVS parameter
%       -'RLS_pars': values for RLS parameter
%       -'smooth_par' (or 'l2_par'): value of the smoothing parameter (default is 0)
%       -'err_type': 'regr'(deafult) for regression, 'class' for 
%        classfication
%       -'1step': (default is false) if true evaluates error of the model
%        learned via NVS without the RLS step.
%       -'2steps': (default is true) if true evaluates error of the model
%       learned via NVS with the RLS step.
%       -'K': integer for performing sequential K-fold cross-validation.
%        If K=0 or K=length(Y) it performs LOO cross-validation
%       -'rand_split': if false (default) perfoms sequential K-fold
%        CV, otherwise performs random K-fold CV.
%       -'plot': (default is false) if true plots training, and validation 
%        errors, and number of selected variables vs the NVS parameter.
%
%   CV_OUTPUT's fields
%	-sparsity: number of selected features for each value of the NVS parameter
% 	-selected_all: indexes of the selected features for each value of the
% 	 NVS parameter.
%   if '1step'==true:
%       -tau_opt_1step: NVS parameter minimizing the K-fold
%        cross-validation error for the 1-step framework (NVSl2 only)
%       -err_KCV_1step: cross-validation error on validation set for for
%        the 1-step framework 
%       -err_train_1step: cross-validation error on training set for the
%        1-step framework 
%   if '2steps'==true:
%       -tau_opt_2steps: NVS parameter minimizing the K-fold
%        cross-validation error for the 2-steps framework (NVSl2 and RLS)
%       -lambda_opt_2steps: RLS parameter minimizing the K-fold
%        cross-validation error for the 2-steps framework
%       -err_KCV_2steps: cross-validation error on validation set for the
%        2-steps framework
%       -err_train_2steps: cross-validation error on training set for the
%        2-steps framework
%
%   MODEL's fields
%   -ker: kernel type;
%   -ker_par: kernel parameter;
%   -Xtr: data matrix used for training
%   if '1step'==true:
%       -selected_1step: indexes of the selected features for the optimal
%        parameters  for the 1-step framework
%       -fun_1step: regression function for the 1-step framework. It's a 
%        2x1 cell array, where the first cell is the nx1 coefficient vector
%        for matrix K, and the second cell is the n*dx1 coefficient vector
%        for matrix Z.
%   if '2steps'==true:
%       -selected_2steps: indexes of the selected features for the optimal
%        parameters for the 2-steps framework
%       -fun_2steps: regression function for the 2-steps framework. It's a
%        nx1 coefficent vector for matrix K.
%
%   See also NVS_ALGORITHM, NVS_REGPATH, RLS_ALGORITHM
%
% MG 1/1/2018 added using validation sample instead of cross-validation
% MG 3/1/2018 replaced missing rls_ker_regpath funciton by kr_ridge_regPath
% MG 3/1/2018 added storing of optimal tau and lambda index

if nargin<2, error('too few input!'), end

% DEFAULT PARAMETRS
err_type = 'regr';
ker = 'pol';
ker_par = 2;
smooth_par = 0;
ntau = 100;
tau_min = [];
tau_max = []; 
tau_values = [];
lambda_values = [];
K = 0;
split = false;
plotting = false;  
twosteps = true;
onestep = false;

% OPTIONAL PARAMETERS
args = varargin;
nargs = length(args);
for i=1:2:nargs
    switch args{i},
        case 'kernel';
            ker = args{i+1};
        case 'kernel_par';
            ker_par = args{i+1};
		case 'NVS_pars'
            tau_values = args{i+1};
		case 'NVS_min_par'
            tau_min = args{i+1};
		case 'NVS_max_par'
            tau_max = args{i+1};
		case 'NVS_n_par'
            ntau = args{i+1};
 		case 'RLS_pars'
            lambda_values = args{i+1};
		case 'err_type'
            err_type = args{i+1};
		case 'smooth_par'
            smooth_par = args{i+1};
		case 'K'
            K = args{i+1};
		case 'rand_split'
            split = args{i+1};
		case 'plot'
            plotting = args{i+1};
		case '2steps'
            twosteps = args{i+1};
		case '1step'
            onestep = args{i+1};
    case 'expName'
            expName = args{i+1};
    end
end


% given the parameters range, determines possible values
[Kmat,Zmat,Lmat] = nvs_kernel_matrices(X,ker,ker_par);

if isempty(tau_values);
    if isempty(tau_max);
        tau_max = nvs_tau_max(Lmat,Kmat,Y);
    end
    if isempty(tau_min);
        tau_min = tau_max/100;
    end
    tau_values = [tau_min tau_min*((tau_max/tau_min)^(1/(ntau-1))).^(1:(ntau-1))]; %geometric series.
else
    ntau = length(tau_values);
end
if and(isempty(lambda_values),twosteps);
    sigma = normest(Kmat);
    lambda_values = sigma*(10.^(-9.8:.2:0));
end
clear Kmat Zmat Lmat
cv_output.tau_values = tau_values;
cv_output.lambda_values = lambda_values;

if K==1
% MG 1/1/2018
	sets{1} = [];
else
	sets = splitting(Y,K,split); %splits the training set in K subsets
end

% initialization
if onestep
    err_KCV = zeros(length(sets),ntau);
    err_train = zeros(length(sets),ntau);
end
if twosteps
    err_KCV2 = zeros(length(sets),ntau,length(lambda_values));
    err_train2 = zeros(length(sets),ntau,length(lambda_values));
end
selected = cell(length(sets),1);
sparsity = zeros(length(sets),ntau);

for i = 1:length(sets);
    
    ind = setdiff(1:length(Y),sets{i}); %indexes of training set
    
    Xtr = X(ind,:);
    Ytr = Y(ind);
 		if K==1
		% MG 1/1/2018
      tmp = load(fullfile('Experiments/Data/',expName),'valid');
		  Xval = tmp.valid.X;
		  Yval = tmp.valid.y;
    else
      Xval = X(sets{i},:);
      Yval = Y(sets{i});
    end

    % entire solution path
    if onestep
        [selected{i},out2,tmin,f1,f2] = nvs_regpath(Xtr,Ytr,tau_values,'kernel',ker,'kernel_par',ker_par,'smooth_par',smooth_par);
        clear out2
    else
        selected{i} = nvs_regpath(Xtr,Ytr,tau_values,'kernel',ker,'kernel_par',ker_par,'smooth_par',smooth_par);
    end
    selected{i} = selected{i}~=0;
    sparsity(i,:) = sum(selected{i});

    [Kval,Zval] = nvs_kernel_matrices_test(Xtr,Xval,ker,ker_par);
    [Ktr,Ztr] = nvs_kernel_matrices_test(Xtr,Xtr,ker,ker_par);
        
    if onestep
        for t = tmin:ntau;
            err_KCV(i,t) = nvs_test(Kval,Zval,Yval,f1{t},f2{t},err_type);       
            err_train(i,t) = nvs_test(Ktr,Ztr,Ytr,f1{t},f2{t},err_type);       
        end
        % MG 3/1/2017 fill in missing err_KCV below tmin
        err_KCV(i,1:tmin-1) = repmat(err_KCV(i,tmin),1,tmin-1);
        err_train(i,1:tmin-1) = repmat(err_train(i,tmin),1,tmin-1);
    end
    
    if twosteps
    % evaluates validation error with kernel rls
        for t = 1:ntau;
            K_reduced = kernel_matrix(ker,ker_par,Xtr(:,selected{i}(:,t)));
            Kval_reduced = kernel_matrix(ker,ker_par,Xtr(:,selected{i}(:,t)),Xval(:,selected{i}(:,t)));
            % MG 3/1/2018
            %alpha = rls_ker_regpath(K_reduced,Ytr,lambda_values);
            aTemp = kr_ridge_regPath(K_reduced,Ytr,lambda_values);
            alpha = num2cell(aTemp,1);
            for l = 1:length(lambda_values);
                err_KCV2(i,t,l) = nvs_test(Kval_reduced,0,Yval,alpha{l},0,err_type);   
                err_train2(i,t,l) = nvs_test(K_reduced,0,Ytr,alpha{l},0,err_type);  
            end
        end
    end
end
cv_output.selected_all = selected;
cv_output.sparsity = mean(sparsity);
clear selected sparsity

if onestep
    % evaluate avg. error over the splits
    err_KCV = reshape(mean(err_KCV,1),ntau,1);
    err_train = reshape(mean(err_train,1),ntau,1);
    
    % find NVS parameter minimizing the error
    cv_output.t_opt = find(err_KCV==min(err_KCV),1,'last');
    cv_output.tau_opt_1step = tau_values(cv_output.t_opt);
    cv_output.err_KCV_1step = err_KCV;
    cv_output.err_train_1step = err_train;
end

if twosteps
    % evaluate avg. error over the splits
    err_KCV2 = reshape(mean(err_KCV2,1),ntau,length(lambda_values));
    err_train2 = reshape(mean(err_train2,1),ntau,length(lambda_values));
     % for each NVS parameter, find rls parameter minimizing the error
    l_opt = zeros(ntau,1);
    lambda_opt = zeros(ntau,1);
    err_KCV_opt2 = zeros(ntau,1);
    err_train_opt2 = zeros(ntau,1);
    for t = 1:ntau;
        l_opt(t) = find(err_KCV2(t,:)==min(err_KCV2(t,:)),1,'last');
        lambda_opt(t) = lambda_values(l_opt(t));
        err_KCV_opt2(t) = err_KCV2(t,l_opt(t));
        err_train_opt2(t) = err_train2(t,l_opt(t));
    end
    
    % find NVS parameter minimizing the validation error
    cv_output.t_opt2 = find(err_KCV_opt2==min(err_KCV_opt2),1,'last');
    cv_output.l_opt2 = l_opt(cv_output.t_opt2);
    cv_output.tau_opt_2steps = tau_values(cv_output.t_opt2);
    cv_output.lambda_opt_2steps = lambda_opt(cv_output.t_opt2);
    cv_output.err_KCV_2steps = err_KCV2;
    cv_output.err_train_2steps = err_train2;
end

if nargout>1   
    % build model for future prediction
    model.ker = ker;
    model.ker_par = ker_par;
    %model.Xtrain = X;
    
    if onestep;
        [selected,out1,out2,f1,f2] = nvs_regpath(X,Y,tau_values(cv_output.t_opt),'kernel',ker,'kernel_par',ker_par,'smooth_par',smooth_par,'all_path',false); 
        clear out1 out2
        model.selected_1step = selected~=0;
        model.fun_1step = {f1;f2};
    end

    if twosteps;
        selected = nvs_regpath(X,Y,tau_values(cv_output.t_opt2),'kernel',ker,'kernel_par',ker_par,'smooth_par',smooth_par,'all_path',false)~=0; 
        Kmat = kernel_matrix(ker,ker_par,X(:,selected));
        %alpha_rls = rls_ker_algorithm(Kmat,Y,lambda_opt(t_opt2));    
        alpha_rls = kr_ridge_regPath(Kmat,Y,lambda_opt(cv_output.t_opt2));

        model.selected_2steps = selected;
        model.fun_2steps = alpha_rls;    
    end
end

if plotting;
    figure('Name','Nonlinear Variable Selection')
    c = 0;
    if onestep
        c = c+1;
        subplot(twosteps+onestep+1,1,c)
        semilogx(tau_values,err_train,'b*-'); hold on;
        semilogx(tau_values,err_KCV,'r*-'); 
        legend('train','validation');
        semilogx(tau_values,repmat(min(err_train), ntau,1),'b--')
        semilogx(tau_values,repmat(min(err_KCV), ntau,1),'r--')
        title('CV error without second step');
    end
    if twosteps;
        c = c+1;
        subplot(twosteps+onestep+1,1,c)
        semilogx(tau_values,err_train_opt2,'b*-'); hold on;
        semilogx(tau_values,err_KCV_opt2,'r*-'); 
        legend('train','validation');
        semilogx(tau_values,repmat(min(err_train_opt2), ntau,1),'b--')
        semilogx(tau_values,repmat(min(err_KCV_opt2), ntau,1),'r--')
        title('CV error with RLS');
    end
    c = c+1;
    subplot(twosteps+onestep+1,1,c)
    semilogx(tau_values,cv_output.sparsity,'*-'); 
    xlabel('\tau');
    title('# of selected variables');
end
