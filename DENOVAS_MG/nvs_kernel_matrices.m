function[K,Z,L] = nvs_kernel_matrices(X,ker,ker_par)
%KERNEL_MATRICES Creates kernel matrices
%   [K,Z,L] = NVS_KERNEL_MATRICES(X,KER,KER_PAR) Creates the kernel
%   matrices necessary for NVS algorithm with input data X.
%
% NB: per Z, sul secondo indice le prime n componenti si riferiscono alla 
% variabile 1, le seconde n alla variabile 2 e cosi' via
%
% NB: per L, sia sul primo sia sul secondo indice, le prime componenti n si 
% riferiscono alla variabile 1, le secondie n alla variabile 2 e cosi' via

if nargin<2; error('too few inputs!'); end
if and(nargin<3,~strcmp(ker,'lin'));  error('kernel parameter is missing!'); end
if nargin>3; error('too many inputs!'); end

switch ker
    case 'lin'
        Z = z_lin(X);
        L = l_lin(X);
        K = k_lin(X);
    case 'gauss'
        fprintf('\n building matrix Z...')
        Z = z_gauss(X,ker_par);
        fprintf('\t matrix Z built\n')
        fprintf('\n building matrix L...')
        L = l_gauss(X,ker_par);
        fprintf('\t matrix L built\n')
        fprintf('\n building matrix K...')
        K = k_gauss(X,ker_par);
        fprintf('\t matrix K built\n')
    case 'pol'
        fprintf('\n building matrix Z...')
        Z = z_pol(X,ker_par);
        fprintf('\t matrix Z built\n')
        fprintf('\n building matrix L...')
        L = l_pol(X,ker_par);
        fprintf('\t matrix L built\n')
        fprintf('\n building matrix K...')
        K = k_pol(X,ker_par);
        fprintf('\t matrix K built\n')
    otherwise
        error('Unknown kernel')
end

function K = k_gauss(X,sigma)
% NB e' equivalente a:
% n = size(X,1);
% [x y]=meshgrid(1:n,1:n);
% x=x(:);
% y=y(:);
% var=(2*(sigma^2));
% % calcolo l'esponenziale per ogni coppia
% K=exp(-sum((X(y,:)-X(x,:)).^2,2)/var);
% % riordino il risultato in una matrice
% K = reshape(K,n,n);
n = size(X,1);
K = zeros(n);
var=(2*(sigma^2));
for i = 1:n; 
    for j=1:n;
        K(i,j) = exp(-norm(X(i,:)-X(j,:))^2/var);
    end; 
end;

function Z = z_gauss(X,sigma)
% NB e' equivalente a:
% [n,d] = size(X);
% var = 2*sigma^2;
% [x1,x2,x3] = ndgrid(1:n,1:n,1:d);
% x1 = x1(:);
% x2 = x2(:);
% x3 = x3(:);
% Z = exp(-sum((X(x1,:)-X(x2,:)).^2,2)/var);
% X = X(:);
% Z = Z.*(X(x1+(x3-1)*n)-X(x2+(x3-1)*n))/sigma^2;
% Z = reshape(Z,n,n*d);
[n,d] = size(X);
Z = zeros(n,n*d);
for i = 1:n; 
    for a=1:d; 
        for j=1:n;
            Z(i,(a-1)*n+j) = 1/sigma^2*(X(i,a)-X(j,a))*exp(-1/(2*sigma^2)*norm(X(i,:)-X(j,:))^2);
        end; 
    end; 
end;

function L = l_gauss(X,sigma)
% NB e' equivalente a:
% [n,d] = size(X);
% var = 2*sigma^2;
% [x1,x2,x3,x4] = ndgrid(1:n,1:d,1:n,1:d);
% x1 = x1(:);
% x2 = x2(:);
% x3 = x3(:);
% x4 = x4(:);
% L = exp(-sum((X(x1,:)-X(x3,:)).^2,2)/var)./sigma^4;
% X = X(:);
% L = L.*((x2==x4).*sigma^2-(X(x1+(x2-1)*n)-X(x3+(x2-1)*n)).*(X(x1+(x4-1)*n)-X(x3+(x4-1)*n)));
% L = reshape(L,n*d,n*d);

[n,d] = size(X);
dist = zeros(n,n);
for i = 1:n;
    for j =1:n;
        dist(i,j) = exp(-1/(2*sigma^2)*norm(X(i,:)-X(j,:))^2)./(sigma^4);
    end
end
X1 = repmat(cell2mat(mat2cell(repmat(reshape(X,n*d,1),1,n),n*ones(d,1),n)'),d,1);
X2 = repmat(reshape(X,n*d,1),1,n*d);
L = -(X2-X1').*(X1-X2').*repmat(dist,d,d);
for a=1:d;
    L(((a-1)*n+1):(a*n),((a-1)*n+1):(a*n)) = L(((a-1)*n+1):(a*n),((a-1)*n+1):(a*n)) + (sigma^2).*dist;
end

% [n,d] = size(X);
% L = zeros(n*d,n*d);
% for i = 1:n; 
%     for a=1:d; 
%         for j=1:n;
%             for b = 1:d;
%                 if a~=b;
%                     L((a-1)*n+i,(b-1)*n+j) = -1/sigma^4*(X(i,a)-X(j,a))*(X(i,b)-X(j,b))*exp(-1/(2*sigma^2)*norm(X(i,:)-X(j,:))^2);
%                 else
%                     L((a-1)*n+i,(b-1)*n+j) = 1/sigma^4*(sigma^2-(X(i,a)-X(j,a))^2)*exp(-1/(2*sigma^2)*norm(X(i,:)-X(j,:))^2); 
%                 end
%             end
%         end; 
%     end; 
% end; 

function K = k_pol(X,p)
K = (X*X'+1).^p;

function Z = z_pol(X,p)
% NB e' equivalente a:
% [n,d] = size(X);
% [x1,x2,x3] = ndgrid(1:n,1:n,1:d);
% x1 = x1(:);
% x2 = x2(:);
% x3 = x3(:);
% Z = p*(sum(X(x1,:).*X(x2,:),2)+1).^(p-1);
% X = X(:);
% Z = Z.*X(x1+(x3-1)*n);
% Z = reshape(Z,n,n*d);
[n,d] = size(X);
Z = zeros(n,n*d);
for i = 1:n; 
    for a=1:d; 
        for j=1:n; 
            Z(i,(a-1)*n+j) = p*(sum(X(i,:).*X(j,:))+1)^(p-1)*X(i,a); 
        end; 
    end; 
end

function L = l_pol(X,p)
% NB e' equivalente a: NO e' da ricontrollare!!!
% [n,d] = size(X);
% [x1,x2,x3,x4] = ndgrid(1:n,1:d,1:n,1:d);
% x1 = x1(:);
% x2 = x2(:);
% x3 = x3(:);
% x4 = x4(:);
% L = (p-1)*p*(sum(X(x1,:).*X(x3,:),2)+1).^(p-2);
% Ldiag =p*(sum(X(x1,:).*X(x3,:),2)+1).^(p-1).*(x2==x4);
% Xl = X(:);
% L = L.*Xl(x3+(x2-1)*n).*Xl(x1+(x4-1)*n)+Ldiag;
% L = reshape(L,n*d,n*d);

[n,d] = size(X);
XX = repmat(cell2mat(mat2cell(repmat(reshape(X,n*d,1),1,n),n*ones(d,1),n)'),d,1);
L = repmat(((p-1)*p).*(X*X'+1).^(p-2),d,d).*XX.*XX';
for a=1:d;
    L(((a-1)*n+1):(a*n),((a-1)*n+1):(a*n)) = L(((a-1)*n+1):(a*n),((a-1)*n+1):(a*n)) + p.*(X*X'+1).^(p-1);
end
% [n,d] = size(X);
% L = zeros(n*d,n*d);
% for i = 1:n; 
%     for a=1:d; 
%         for j=1:n;
%             for b = 1:d;
%                 if a~=b;
%                     L((a-1)*n+i,(b-1)*n+j) = (p-1)*p*(sum(X(i,:).*X(j,:))+1)^(p-2)*X(j,a)*X(i,b);
%                 else
%                     L((a-1)*n+i,(b-1)*n+j) = (p-1)*p*(sum(X(i,:).*X(j,:))+1)^(p-2)*X(j,a)*X(i,b)+ p*(sum(X(i,:).*X(j,:))+1)^(p-1); 
%                 end
%             end
%         end; 
%     end; 
% end;  

function K = k_lin(X)
K = X*X';

function Z = z_lin(X)
[n,d] = size(X);
Z = zeros(n,n*d);
for i = 1:n; 
    for a=1:d; 
        for j=1:n; 
            Z(i,(a-1)*n+j) = X(i,a); 
        end; 
    end; 
end

function L = l_lin(X)
[n,d] = size(X);
L = zeros(n*d,n*d);
for i = 1:n; 
    for a=1:d; 
        for j=1:n;
            for b = 1:d;
                if a==b;
                    L((a-1)*n+i,(b-1)*n+j) = 1;
                end
            end
        end; 
    end; 
end; 