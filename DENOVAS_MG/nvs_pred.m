function pred = nvs_pred(model,Xtest,Ytest,err_type)
%Predicts labels on test set
%   [PRED] = NVS_PRED(MODEL,XTEST) Given MODEL from NVS_KCV predicts labels
%   for test set Xtest. 
%   PRED's fields:
%       -Y_1STEP(if MODEL contains field FUN_1STEP)
%       -Y_2STEPS(if MODEL contains field FUN_2STEPS)
%   [PRED] = NVS_PRED(MODEL,XTEST,YTEST,ERR_TYPE) Given MODEL from 
%   NVS_KCV predicts labels and compute means square errror for test set XTEST. 
%   PRED's fields:
%       if MODEL contains field FUN_1STEP
%           -Y_1STEP: estimated labels
%           -ERR_1STEP: mean square error
%       if MODEL contains field FUN_2STEPS
%           -Y_2STEPS:estimated labels
%           -ERR_2STEPS: mean square error 
% 
%   See also NVS_KCV


if isfield(model,'fun_1step')
    [Ktest,Ztest] = nvs_kernel_matrices_test(model.Xtrain(:,model.selected_2steps),Xtest(:,model.selected_2steps),model.ker,model.ker_par);
    pred.y_1step = Ktest*model.fun_1step{1}+Ztest*model.fun_1step{2};
end
if isfield(model,'fun_2steps')
    Ktest = kernel_matrix(model.ker,model.ker_par,model.Xtrain(:,model.selected_2steps),Xtest(:,model.selected_2steps));
    pred.y_2steps = Ktest*model.fun_2steps;
end

if nargin>2;
    npos = sum(Ytest>0);
    nneg = sum(Ytest<0);
        
    % if class weight is not given let false positive and false negative 
    % weight the same error     
    if strcmp(err_type,'class'); 
        err_type = [npos/(npos+nneg) nneg/(npos+nneg)]; 
    end

    % if class weight is given (e.g. ERR_TYPE = [1/2 1/2]) compute FP and 
    % FN rate and then compute weighted mean with CLASS_FRACTION
    if isnumeric(err_type)
        class_fraction = err_type;
        
        if isfield(model,'fun_1step')
            pred.y_1step = sign(pred.y_1step);
            pred.err_1step = 0;
            if npos>0;
                err_pos = sum((pred.y_1step(Ytest>0)~=sign(Ytest(Ytest>0))))/npos;
                pred.err_1step = pred.err_1step + err_pos*max(class_fraction(1),nneg==0);
            end
            if nneg>0;
                err_neg = sum((pred.y_1step(Ytest<0)~=sign(Ytest(Ytest<0))))/nneg;
                pred.err_1step = pred.err_1step + err_neg*max(class_fraction(2),npos==0);
            end
        end
        
        if isfield(model,'fun_2steps')
            pred.y_2steps = sign(pred.y_2steps);
            pred.err_2steps = 0;
            if npos>0;
                err_pos = sum((pred.y_2steps(Ytest>0)~=sign(Ytest(Ytest>0))))/npos;
                pred.err_2steps = pred.err_2steps + err_pos*max(class_fraction(1),nneg==0);
            end
            if nneg>0;
                err_neg = sum((pred.y_2steps(Ytest<0)~=sign(Ytest(Ytest<0))))/nneg;
                pred.err_2steps = pred.err_2steps + err_neg*max(class_fraction(2),npos==0);
            end
        end
        
    elseif isequal(err_type,'regr')
        if isfield(model,'fun_1step')    
            pred.err_1step = norm(pred.y_1step-(Ytest))^2/length(Ytest);
        end
        if isfield(model,'fun_2steps')    
            pred.err_2steps = norm(pred.y_2steps-(Ytest))^2/length(Ytest);
        end
    end
end