function[K] = kernel_matrix(ker,ker_par,X,Xtest)
%KERNEL_MATRIX Creates kernel matrices
%   MG: had to create this cause was missing in Rosasco's zip
%   K = KERNEL_MATRIX(KER,KER_PAR,X,XTEST) Creates the kernel
%   matrices necessary for testing NVS solution with trainig matrix X and
%   test matrix Xtest.
%

if nargin<3; error('too few inputs!'); end
if nargin==3; Xtest=X; end
if nargin>4; error('too many inputs!'); end

switch ker
    case 'lin'
        K = k_lin(X,Xtest);
    case 'gauss'
        fprintf('\n building matrix K...')
        K = k_gauss(X,Xtest,ker_par);
        fprintf('\t matrix K built\n')
    case 'pol'
        fprintf('\n building matrix K...')
        K = k_pol(X,Xtest,ker_par);
        fprintf('\t matrix K built\n')
    otherwise
        error('Unknown kernel')
end

function K = k_gauss(X,Xtest,sigma)
n = size(X,1);
ntest = size(Xtest,1);
K = zeros(ntest,n);
var=(2*(sigma^2));
for i = 1:ntest; 
    for j=1:n;
        K(i,j) = exp(-norm(Xtest(i,:)-X(j,:))^2/var);
    end; 
end;

function K = k_pol(X,Xtest,p)
K = (Xtest*X'+1).^p;

function K = k_lin(X,Xtest)
K = Xtest*X';