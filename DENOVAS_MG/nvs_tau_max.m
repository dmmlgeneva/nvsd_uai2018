function tau_max = nvs_tau_max(L,K,Y)
tau_max = 0;
n = length(Y);
d = round(size(L,1)/n);
v = ones(n,1)/sqrt(n);
Y = Y-mean(Y);
top = sqrt((Y'*K*Y)/n);
for j = 1:d;
    bottom = sqrt(v'*L(((j-1)*n+1):(j*n),((j-1)*n+1):(j*n))*v);
    tau_max = max(tau_max,top/bottom);
end