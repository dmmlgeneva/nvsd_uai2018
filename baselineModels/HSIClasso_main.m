function HSIClasso_main(expName,kernelType,kernelParam)
% NVSD_LASSO_MAIN - nvsd lasso results
%
% INPUTS
%   expName - name of experiment
%   kernelType - type kernel (pol,polHom,gauss,lin)
%   kernelParam - kernel parameter (specific for kernel type)
%
% OUTPUTS
%   saves HSIClasso results to Experiments/Results/expName
%
% EXAMPLE: HSIClasso_main('BH_001','gauss')
%
% CREATED: MG 7/1/2017

mainTic = tic;
fprintf('Running HSIC lasso for %s\n',expName);

load(fullfile('Experiments/Data/',expName));
[n,d] = size(train.X);

%% fill in kernel Param
% guaussian width
if (strcmp(kernelType,'gauss') || strcmp(kernelType,'gausn')) && (~exist('kernelParam','var') || isempty(kernelParam)),
  kernelParam = gaussWidth(train.X,train.y);
% empty for linear kernel
elseif strcmp(kernelType,'lin')
  kernelParam = [];
end
kPOrig = kernelParam;
if strcmp(kernelType,'gausn')
  kernelParam = kernelParam/d;
end

%% 50-long lambda grid
sigma = normest(train.X'*train.X);
lastaus = sigma*(10.^(-9.8:.2:0));

%% HSIC feature selection
ll = length(lastaus);
for tIdx = 1:length(lastaus)
  slct(:,tIdx) = logical(max(0,HSICLasso(train.X',train.y',1,lastaus(tIdx)))); % max 0 to avoid nans 
end

%% 2nd step fitting by kernel ridge
for tIdx = 1:ll;
  [~,interIdx] = intersect(slct',slct(:,tIdx)','rows');
  if interIdx>0 && interIdx~=tIdx
    % copy results if same sparse pattern
    ar{tIdx} = ar{interIdx};
    cvErrs.train_step2(tIdx,:) = cvErrs.train_step2(interIdx,:);
    cvErrs.valid_step2(tIdx,:) = cvErrs.valid_step2(interIdx,:);
  elseif sum(slct(:,tIdx))==0
    % mean prediction for completely sparse model
    ar{tIdx} = zeros(n,length(lastaus));
    [~,cvErrs.train_step2(tIdx,:)] = predict_Linear(train.X,train.y,zeros(size(train.X,2),1));
    [~,cvErrs.valid_step2(tIdx,:)] = predict_Linear(valid.X,valid.y,zeros(size(valid.X,2),1));
  else
    % kr_ridge model
    K = kernelMatrices(train.X(:,slct(:,tIdx)),train.X(:,slct(:,tIdx)),kernelType,kernelParam);
    ar{tIdx} = kr_ridge_regPath(K,train.y,lastaus);
    [~,cvErrs.train_step2(tIdx,:)] = predict_Kr(K,train.y,ar{tIdx});
    K = kernelMatrices(train.X(:,slct(:,tIdx)),valid.X(:,slct(:,tIdx)),kernelType,kernelParam);
    [~,cvErrs.valid_step2(tIdx,:)] = predict_Kr(K,valid.y,ar{tIdx});
  end
end

%% 2nd step avg errors and optimal tau from avg validation errors
% min valid errror across tau lambda
minValidError_step2 = min(min(cvErrs.valid_step2));
% optimal tau lbda indexes (work over transpose to give preference to higher tau)
minIdxT = find(cvErrs.valid_step2'==minValidError_step2,1,'last');
[HSIC_lss.lbdOptIdx_step2,HSIC_lss.tauOptIdx_step2] = ind2sub(size(cvErrs.valid_step2'),minIdxT);
HSIC_lss.tauOpt_step2 = lastaus(HSIC_lss.tauOptIdx_step2);
HSIC_lss.lbdOpt_step2 = lastaus(HSIC_lss.lbdOptIdx_step2);

%% final models
% 2nd step 
HSIC_lss.slct_step2 = full(slct(:,HSIC_lss.tauOptIdx_step2));
HSIC_lss.params.a_step2 = ar{HSIC_lss.tauOptIdx_step2}(:,HSIC_lss.lbdOptIdx_step2);

%% test results
% 2nd step
if sum(HSIC_lss.params.a_step2)==0
  % mean prediction for completely sparse model
  [HSIC_lss.preds.train_step2,HSIC_lss.errs.train_step2] = predict_Linear(train.X,train.y,zeros(size(train.X,2),1));
  [HSIC_lss.preds.test_step2,HSIC_lss.errs.test_step2] = predict_Linear(test.X,test.y,zeros(size(test.X,2),1));
else
  % kr_ridge model
  K = kernelMatrices(train.X(:,HSIC_lss.slct_step2),train.X(:,HSIC_lss.slct_step2),kernelType,kernelParam);
  [HSIC_lss.preds.train_step2,HSIC_lss.errs.train_step2] = predict_Kr(K,train.y,HSIC_lss.params.a_step2);
  K = kernelMatrices(train.X(:,HSIC_lss.slct_step2>0),test.X(:,HSIC_lss.slct_step2),kernelType,kernelParam);
  [HSIC_lss.preds.test_step2,HSIC_lss.errs.test_step2] = predict_Kr(K,test.y,HSIC_lss.params.a_step2);
end

HSIC_lss.cvSlct = slct;
HSIC_lss.cvErrs = cvErrs;
HSIC_lss.lastuas = lastaus;
HSIC_lss.kernelParam=kPOrig;
HSIC_lss.elapsedTime_cv=toc(mainTic);

%% choose the kernel function and save results
switch kernelType
  case 'pol'
    HSIC_lss_pol = HSIC_lss;
    % save results
    resFile = fullfile('Experiments/Results',[expName,'.mat']);
    if exist(resFile, 'file') == 2
      save(resFile,'HSIC_lss_pol','-append');
    else
      save(resFile,'HSIC_lss_pol');
    end
  case 'polHom'
    HSIC_lss_polHom = HSIC_lss;
    % save results
    resFile = fullfile('Experiments/Results',[expName,'.mat']);
    if exist(resFile, 'file') == 2
      save(resFile,'HSIC_lss_polHom','-append');
    else
      save(resFile,'HSIC_lss_polHom');
    end
  case 'gauss'
    HSIC_lss_gauss = HSIC_lss;
    % save results
    resFile = fullfile('Experiments/Results',[expName,'.mat']);
    if exist(resFile, 'file') == 2
      save(resFile,'HSIC_lss_gauss','-append');
    else
      save(resFile,'HSIC_lss_gauss');
    end
  case 'gausn'
    HSIC_lss_gausn = HSIC_lss;
    % save results
    resFile = fullfile('Experiments/Results',[expName,'.mat']);
    if exist(resFile, 'file') == 2
      save(resFile,'HSIC_lss_gausn','-append');
    else
      save(resFile,'HSIC_lss_gausn');
    end
  case 'lin'
    HSIC_lss_lin = HSIC_lss;
    % save results
    resFile = fullfile('Experiments/Results',[expName,'.mat']);
    if exist(resFile, 'file') == 2
      save(resFile,'HSIC_lss_lin','-append');
    else
      save(resFile,'HSIC_lss_lin');
    end
  otherwise
    error('unknown kernel')
end


fprintf('Total time = %f\n',toc(mainTic));

end