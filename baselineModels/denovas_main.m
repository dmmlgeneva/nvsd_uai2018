function denovas_main(expName,kFolds,kernelType,kernelParam,smooth_par)
% DENOVAS_MAIN - call deonvas algorithm (Rosasco2013)
%
% INPUTS
%   expName - name of experiment
%   kfolds - integer number of cross validation samples (default=5)
%   kernelType - type kernel (pol,polHom,gauss,lin)
%   kernelParam - kernel parameter (specific for kernel type)
%   smooth_par - deonvas smoothing parameter
%
% OUTPUTS
%   saves denovas results to Experiments/Results/expName
%
% EXAMPLE: denovas_main('BH_001',5)
%
% CREATED: MG 3/1/2018

tic
fprintf('Running denovas for %s',expName);

%% fill in optional parameters
if ~exist('kfolds','var') || isempty(kfolds),
  kfolds = 5;
end
if ~exist('smooth_par','var') || isempty(smooth_par),
  smooth_par = 0;
end

load(fullfile('Experiments/Data/',expName));


%% fill in kernel Param
% guaussian width
if (strcmp(kernelType,'gauss') || strcmp(kernelType,'gausn')) && (~exist('kernelParam','var') || isempty(kernelParam)),
  kernelParam = gaussWidth(train.X,train.y);
% empty for linear kernel
elseif strcmp(kernelType,'lin')
  kernelParam = [];
end
if strcmp(kernelType,'gausn')
  kernelType='gauss';
  gausn=1;
else 
  gausn=0;
end

% cross validation with sparsity learning followed by ridge debiasing
[output,model] = nvs_kcv(train.X,train.y,'plot',false,'K',kFolds,'kernel',kernelType,'kernel_par',kernelParam,'smooth_par',smooth_par,'1step',1,'2steps',1,'expName',expName);

%% test results
% 1st step
[K,Z] = nvs_kernel_matrices_test(train.X,train.X,kernelType,kernelParam);
[dnvs.errs.train,dnvs.preds.train] = nvs_test(K,Z,train.y,model.fun_1step{1},model.fun_1step{2},'regr');
[K,Z] = nvs_kernel_matrices_test(train.X,test.X,kernelType,kernelParam);
[dnvs.errs.test,dnvs.preds.test] = nvs_test(K,Z,test.y,model.fun_1step{1},model.fun_1step{2},'regr');
% 2nd step
kPOrig = kernelParam;
if gausn
  kernelParam = kernelParam/size(train.X,2)*sum(model.selected_2steps);
end
K = kernel_matrix(kernelType,kernelParam,train.X(:,model.selected_2steps));
[dnvs.errs.train_step2,dnvs.preds.train_step2] = nvs_test(K,0,train.y,model.fun_2steps,0,'regr');
K = kernel_matrix(kernelType,kernelParam,train.X(:,model.selected_2steps),test.X(:,model.selected_2steps));
[dnvs.errs.test_step2,dnvs.preds.test_step2] = nvs_test(K,0,test.y,model.fun_2steps,0,'regr');

dnvs.output = output;
dnvs.model = model;

dnvs.elapsedTime=toc;

if gausn
  kernelType='gausn';
end

%% choose the kernel function and save results
switch kernelType
  case 'pol'
    denovas_pol = dnvs;
    % save results
    resFile = fullfile('Experiments/Results',[expName,'.mat']);
    if exist(resFile, 'file') == 2
      save(resFile,'denovas_pol','-append');
    else
      save(resFile,'denovas_pol');
    end
  case 'polHom'
    denovas_polHom = dnvs;
    % save results
    resFile = fullfile('Experiments/Results',[expName,'.mat']);
    if exist(resFile, 'file') == 2
      save(resFile,'denovas_polHom','-append');
    else
      save(resFile,'denovas_polHom');
    end
  case 'gauss'
    denovas_gauss = dnvs;
    % save results
    resFile = fullfile('Experiments/Results',[expName,'.mat']);
    if exist(resFile, 'file') == 2
      save(resFile,'denovas_gauss','-append');
    else
      save(resFile,'denovas_gauss');
    end
  case 'gausn'
    denovas_gausn = dnvs;
    % save results
    resFile = fullfile('Experiments/Results',[expName,'.mat']);
    if exist(resFile, 'file') == 2
      save(resFile,'denovas_gausn','-append');
    else
      save(resFile,'denovas_gausn');
    end
  case 'lin'
    denovas_lin = dnvs;
    % save results
    resFile = fullfile('Experiments/Results',[expName,'.mat']);
    if exist(resFile, 'file') == 2
      save(resFile,'denovas_lin','-append');
    else
      save(resFile,'denovas_lin');
    end
  otherwise
    error('unknown kernel')
end

fprintf(', Total elapsed time = %f\n',toc);

end